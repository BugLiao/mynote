# markdown语法


---
## 1. 标题
**--------------------markdown语句---------------------**
```markdown
### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题    
```
**----------------------渲染效果-----------------------**

### 这是三级标题
#### 这是四级标题
##### 这是五级标题
###### 这是六级标题    
---

## 2. 字体
**--------------------markdown语句---------------------**
```markdown
**这是加粗的文字**

*这是倾斜的文字*

***这是斜体加粗的文字***

~~这是加删除线的文字~~
```
**----------------------渲染效果-----------------------**

**这是加粗的文字**

*这是倾斜的文字*

***这是斜体加粗的文字***

~~这是加删除线的文字~~

---
#### 参数可以这样写
**--------------------markdown语句---------------------**
```
  - **`load_chassis`** Load chassis URDF. Default: `true`.
  - **`load_gimbal`** Load gimbal URDF. Default: `true`.
  - **`load_shooter`** Load shooter URDF. Default: `true`.
  - **`load_gazebo`** Load Gazebo params and run Gazebo. Default: `true`.
  - **`use_rm_gazebo`** Use Gazebo params and run Gazebo. Default: `false`.
  - **`paused`** Paused simulation when load Gazbeo. Default: `true`.
```
**----------------------渲染效果-----------------------**
  - **`load_chassis`** Load chassis URDF. Default: `true`.
  - **`load_gimbal`** Load gimbal URDF. Default: `true`.
  - **`load_shooter`** Load shooter URDF. Default: `true`.
  - **`load_gazebo`** Load Gazebo params and run Gazebo. Default: `true`.
  - **`use_rm_gazebo`** Use Gazebo params and run Gazebo. Default: `false`.
  - **`paused`** Paused simulation when load Gazbeo. Default: `true`.

---
## 3. 引用
**--------------------markdown语句---------------------**
```markdown
>这是引用的内容
>>这是引用的内容
>>>>>>>>>>这是引用的内容
```
**----------------------渲染效果-----------------------**
>这是引用的内容
>>这是引用的内容
>>>>>>>>>>这是引用的内容

---
## 4. 分割线
**--------------------markdown语句---------------------**
```markdown
---
***
```
**----------------------渲染效果-----------------------**

---

***

---

## 5. 图片
**--------------------markdown语句---------------------**
```markdown
![狗](dog.jpg)

<img src="dog.jpg" width="50" valign="bottom" /> **[GitHub Pages]**: https://yhatt.github.io/marp-cli-example
 
- <img src="https://icongr.am/octicons/mark-github.svg" width="24" height="24" valign="bottom" /> **[GitHub Pages]**: https://yhatt.github.io/marp-cli-example

```
**----------------------渲染效果-----------------------**

![狗](dog.jpg)

<img src="dog.jpg" width="50" valign="bottom" /> **[GitHub Pages]**: https://yhatt.github.io/marp-cli-example
 
- <img src="https://icongr.am/octicons/mark-github.svg" width="24" height="24" valign="bottom" /> **[GitHub Pages]**: https://yhatt.github.io/marp-cli-example

---
## 6. 超链接
**--------------------markdown语句---------------------**
```markdown
[百度](http://baidu.com)
```
**----------------------渲染效果-----------------------**

[百度](http://baidu.com)


---
## 7. 列表

### 无序列表
**--------------------markdown语句---------------------**
```markdown
- 列表内容
+ 列表内容
* 列表内容
```
**----------------------渲染效果-----------------------**
- 列表内容
+ 列表内容
* 列表内容
---
### 有序列表
**--------------------markdown语句---------------------**
```markdown
1. 列表内容
2. 列表内容
3. 列表内容
   1. 前面三个空格
   2. 嘻嘻嘻
```
**----------------------渲染效果-----------------------**
1. 列表内容
2. 列表内容
3. 列表内容
   1. 前面三个空格
   2. 嘻嘻嘻

---

## 8. 表格
- 文字默认居左  
- 两边加：表示文字居中  
- 右边加：表示文字居右
**注意：** 原生的语法两边都要用 | 包起来。此处省略

**--------------------markdown语句---------------------**
```markdown
第一列|第二列|第三列
--:|:--:|:--:
1|2|3
4|5|6
7|8|9
```
**----------------------渲染效果-----------------------**

第一列|第二列|第三列
--:|:--:|:--:
1|2|3
4|5|6
7|8|9

---
## 9. 代码

### 单行代码
**--------------------markdown语句---------------------**
```markdown
`printf("hello world")`  
```
**----------------------渲染效果-----------------------**

`printf("hello world")`  

---
### 代码块

#### bash
**--------------------markdown语句---------------------**
````markdown
```bash
sudo apt-get install tldr
sudo apt-get install vim
```
````

**----------------------渲染效果-----------------------**

```bash
sudo apt-get install tldr
sudo apt-get install vim
```
---
#### python
**--------------------markdown语句---------------------**
````markdown
```python
  if(i>0):
    i = i + 1
  printf("你好吗") # 备注
  return 0
```
````
**----------------------渲染效果-----------------------**
```python
  if(i>0):
    i = i + 1
  printf("你好吗") # 备注
  return 0
```