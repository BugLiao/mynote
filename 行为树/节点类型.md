# 节点类型
文档撰写：廖伦稼
参考：
    https://behaviortree.github.io/BehaviorTree.CPP/SequenceNode/

---
[toc]
---
## 1、序列节点（Sequences）

**特点：** 从左往右遍历子节点，有一例失败则返回失败，全成功则返回成功

**目前框架提供了三种节点：**

- **Sequence** 
- **ReactiveSequence** 
- **SequenceStar** 

**三种节点的区别：**

Type of ControlNode|子节点返回`FAILURE`|子节点返回 `RUNNING`
:--:|:--:|:--:
Sequence|Restart|Tick again
ReactiveSequence|Restart|Restart
SequenceStar|Tick again|Tick again

- `Restart` 表示整个序列从列表的第一个子节点重新启动。

- `Tick again` 意味着下一次该序列被ticked时，同样的子序列将再次被ticked。之前的兄弟(已经返回SUCCESS)不再ticked。
 
**三种节点的共同规则：** 
1. 在**ticking**第一个子节点之前，节点状态变为RUNNING。

2. 如果一个**子节点**返回`SUCCESS`，它会**ticks**下一个**子节点**。

3. 如果最后一个子节点也返回`SUCCESS`，则所有子节点都将停止并且该序列节点返回`SUCCESS`。

### 1.1 Sequence

这棵树代表了狙击手在电脑游戏中的行为。

![pic](pic/01.png)
```cpp
    status = RUNNING;
    // _index 表示第几个子节点

    while(_index < number_of_children)
    {
        child_status = child[_index]->tick();

        if( child_status == SUCCESS ) {
            _index++;
        }
        else if( child_status == RUNNING ) {
            // keep same index
            return RUNNING;
        }
        else if( child_status == FAILURE ) {
            HaltAllChildren();
            _index = 0;
            return FAILURE;
        }
    }
    // 当所有子节点返回 success. 则这个节点也返回 SUCCESS .
    HaltAllChildren();
    _index = 0;
    return SUCCESS;
```


### 1.2 ReactiveSequence

该节点对于持续检查条件特别有用；但是用户在使用异步子节点时也应该小心，以确保它们不会比预期的更频繁。

![pic](pic/03.png)

`ApproachEnemy`是一个异步操作，它返回 RUNNING 直到它最终完成。

条件`isEnemyVisible`将被多次调用，如果变为假（即“失败”），`ApproachEnemy`则停止。
```cpp
    status = RUNNING;

    for (int index=0; index < number_of_children; index++)
    {
        child_status = child[index]->tick();

        if( child_status == RUNNING ) {
            return RUNNING;
        }
        else if( child_status == FAILURE ) {
            HaltAllChildren();
            return FAILURE;
        }
    }
    // 当所有子节点返回 success. 则这个节点也返回 SUCCESS .
    HaltAllChildren();
    return SUCCESS;
```

### 1.3 SequenceStar

如果不想再次tick已经返回 SUCCESS 的子节点时，就使用这个 ControlNode。

**示例：**

这是一个巡逻代理/机器人，必须只访问位置 A、B 和 C一次。如果操作`GoTo(B)`失败，则不会再次 ticked `GoTo(A)`。
另一方面，`isBatteryOK`必须在每次tick 时检查，因此它的父级必须是`ReactiveSequence`.

![pic](pic/02.png)
```cpp
    status = RUNNING;
    // _index is a private member

    while( index < number_of_children)
    {
        child_status = child[index]->tick();

        if( child_status == SUCCESS ) {
            _index++;
        }
        else if( child_status == RUNNING || 
                 child_status == FAILURE ) 
        {
            // keep same index
            return child_status;
        }
    }
    // all the children returned success. Return SUCCESS too.
    HaltAllChildren();
    _index = 0;
    return SUCCESS;
```
---

## 2、回退节点（Fallback）

这一系列节点在其他框架中称为“选择器（Selector）”或“优先级（Priority）”。
他们的目的是尝试不同的策略，直到我们找到一种“有效”的策略。

**特点：** 从左往右遍历子节点，有一例子节点成功则返回成功，无需继续下一子节点

**目前框架提供了两种节点：**
- Fallback
- ReactiveFallback

**两种节点的共同规则：** 
- 在tick第一个子节点之前，节点状态变为`RUNNING`。

- 如果子节点返回`FAILURE`，则回退会tick下一个子节点。

- 如果最后一个子节点也返回`FAILURE`，则所有子节点都将停止并且序列返回FAILURE。

- 如果子节点返回`SUCCESS`，它会停止并返回`SUCCESS`。所有的子节点都停了下来。


**两种节点的区别：**

Type of ControlNode|子节点返回 `RUNNING`
:--:|:--:
Fallback|Tick again
ReactiveFallback|Restart

### 2.1 Fallback

在这个例子中，我们尝试不同的策略来开门。首先检查门是否打开

![pic](pic/04.png)

```cpp
    // index 在构造函数初始化为0
    status = RUNNING;

    while( _index < number_of_children )
    {
        child_status = child[index]->tick();

        if( child_status == RUNNING ) {
            // 暂停执行并返回RUNNING。
            // 在这一个tick，_index 还是相同值
            return RUNNING;
        }
        else if( child_status == FAILURE ) {
            // c继续这个循环
            _index++;
        }
        else if( child_status == SUCCESS ) {
            // 暂停执行并返回SUCCESS。
            HaltAllChildren();
            _index = 0;
            return SUCCESS;
        }
    }
    // 所有的子节点都返回失败。则该节点也返回失败。
    index = 0;
    HaltAllChildren();
    return FAILURE;
```

### 2.2 ReactiveFallback

如果先前条件之一将其异步子节点的状态状态从 `FAILURE` 更改为 `SUCCESS`，则需要中断异步子节点时使用此ControlNode。

在下面的例子中，如果他/她充分休息，他/她的睡眠时间将达到 8 小时或更短。

![pic](pic/05.png)

```cpp
     status = RUNNING;

    for (int index=0; index < number_of_children; index++)
    {
        child_status = child[index]->tick();

        if( child_status == RUNNING ) {
            // Suspend all subsequent siblings and return RUNNING.
            HaltSubsequentSiblings();
            return RUNNING;
        }

        // if child_status == FAILURE, continue to tick next sibling

        if( child_status == SUCCESS ) {
            // Suspend execution and return SUCCESS.
            HaltAllChildren();
            return SUCCESS;
        }
    }
    // all the children returned FAILURE. Return FAILURE too.
    HaltAllChildren();
    return FAILURE;
```
---
## 3、 修饰节点（Decorators）

**特点：** 修饰节点是一个只能有一个子节点的节点。
由Decorators 决定是否 tick 子节点、何时tick 子节点， tick 子节点多少次。

### 3.1 InverterNode 取反
tick 子节点一次，如果子节点失败则返回 `SUCCESS` 或如果子节点成功则返回 `FAILURE`。

如果子节点返回 RUNNING，则此节点也返回 RUNNING。

### 3.2 ForceSuccessNode
如果子节点返回 `RUNNING`，则此节点也返回 `RUNNING`。

否则，它总是返回 `SUCCESS`。

### 3.3 ForceFailureNode
如果子节点返回 `RUNNING`，则此节点也返回 `RUNNING`。

否则，它总是返回 `FAILURE`。

### 3.4 RepeatNode
只要子节点返回 `SUCCESS`，最多tick子节点 N 次，其中 N 作为Input Port传递。

如果子进程返回 `FAILURE`，则中断循环，在这种情况下，也返回 `FAILURE`。

如果子节点返回 `RUNNING`，则此节点也返回 `RUNNING`。

### 3.5 RetryNode
只要子节点返回 `FAILURE`，最多tick 子节点 N 次，其中 N 作为Input Port传递。

如果子节点程返回 `SUCCESS`，则中断循环，在这种情况下，也返回 `SUCCESS`。

如果子节点返回 `RUNNING`，则此节点也返回 `RUNNING`。












<!-- ## Types of nodes
- **`ControlNodes`** - 是可以有 1 到 N个子节点的节点。一旦接收到一个`tick`，这个`tick`就可以传播到一个或多个子节点。
- **`DecoratorNodes`** - 类似于 `ControlNode`，但只能有一个子节点
- **`ActionNodes`** - 叶子节点，没有任何子节点。用户应该实现自己的 ActionNode 来执行实际任务
- **`ConditionNodes`** - 等价于ActionNodes，但它们总是自动的和同步的，即它们不能返回RUNNING。他们不应该改变系统的状态 -->