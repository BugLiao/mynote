# 行为树的XML格式

## **example:** 
```xml
 <root main_tree_to_execute = "MainTree" >
     <BehaviorTree ID="MainTree">
        <Sequence name="root_sequence">
            <SaySomething   name="action_hello" message="Hello"/>
            <OpenGripper    name="open_gripper"/>
            <ApproachObject name="approach_object"/>
            <CloseGripper   name="close_gripper"/>
        </Sequence>
     </BehaviorTree>
 </root>
```
### 文件解读
- tree的第一个标签是`<root>`,它可能包含1 个或多个标签`<BehaviorTree>`。
- 标签`<BehaviorTree>`应该有属性`[ID]`。
- 标签`<root>`可能包含属性`[main_tree_to_execute]`。如果文件包含多个`<BehaviorTree>`，则该属性`[main_tree_to_execute]`是必需的，否则是可选的。
- 每个 TreeNode 由单个标签表示。特别是：
    - 标签的`name`是用于在factory中注册 TreeNode的ID。
    - 属性`[name]`是指实例的名称, 是可选的。
    - 端口是使用属性配置的。在前面的示例中，操作 SaySomething需要输入端口message。

- 从子节点的数量来看
    - `ControlNodes`包含1 到 N 个子节点。
    - `DecoratorNodes`和 `Subtrees`只包含1 个 子节点。
    - `ActionNodes`和`ConditionNodes` 没有子节点。 