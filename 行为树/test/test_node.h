//
// Created by liao on 2021/8/8.
//

#ifndef BEHAVIORTREE_TEST_NODE_H
#define BEHAVIORTREE_TEST_NODE_H
#include "behaviortree_cpp_v3/behavior_tree.h"
#include "behaviortree_cpp_v3/bt_factory.h"


struct velocity_2D
{
    double x, y, theta;
};

inline void SleepMS(int ms)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(ms));
}

namespace BT
{
// This template specialization is needed only if you want
// to AUTOMATICALLY convert a NodeParameter into a Pose2D
// In other words, implement it if you want to be able to do:
//
//   TreeNode::getInput<Pose2D>(key, ...)
//
    template <> inline
    velocity_2D convertFromString(StringView key)
    {
        // three real numbers separated by semicolons
        auto parts = BT::splitString(key, ';');
        if (parts.size() != 3)
        {
            throw BT::RuntimeError("invalid input)");
        }
        else
        {
            velocity_2D output;
            output.x     = convertFromString<double>(parts[0]);
            output.y     = convertFromString<double>(parts[1]);
            output.theta = convertFromString<double>(parts[2]);
            return output;
        }
    }
} // end namespace BT


namespace TestNode
{

    //================================SaySomething====================================//
    class SaySomething : public BT::SyncActionNode  // 同步，有接口(port)
    {
    public:
        SaySomething(const std::string& name, const BT::NodeConfiguration& config): BT::SyncActionNode(name, config)
        {
        }

        // tick（）的具体实现要在test_node.cpp写
        BT::NodeStatus tick() override;

        // 这个必须要写，指定接口的参数名称 message
        static BT::PortsList providedPorts()
        {
            return{ BT::InputPort<std::string>("message") };
        }
    };


    //=================================Run===================================//
    class Run : public BT::SyncActionNode  // 同步，无接口(port)
    {
    public:
        Run(const std::string& name): BT::SyncActionNode(name, {}) //注意这里无接口和有接口的区别
        {
        }

        BT::NodeStatus tick() override;

    };


    //=================================MoveAction===================================//


    class MoveAction : public BT::AsyncActionNode  // 异步，有接口
    {
    public:
        // Any TreeNode with ports must have a constructor with this signature
        MoveAction(const std::string& name, const BT::NodeConfiguration& config)
                : AsyncActionNode(name, config)
        {
        }

        // It is mandatory to define this static method.
        static BT::PortsList providedPorts()
        {
            return{ BT::InputPort<velocity_2D>("cmd_vel") };
        }

        BT::NodeStatus tick() override;

        virtual void halt() override;

    private:
        std::atomic_bool _halt_requested;
    };

//=================================robot_state===================================//
//    class robot_state : public BT::SyncActionNode  // 同步，有接口(port)
//    {
//    public:
//        robot_state(const std::string& name, const BT::NodeConfiguration& config): BT::SyncActionNode(name, config)
//        {
//        }
//
//        // tick（）的具体实现要在test_node.cpp写
//        BT::NodeStatus tick() override;
//
//        // 这个必须要写，指定接口的参数名称 message
//        static BT::PortsList providedPorts()
//        {
//            return{ BT::InputPort<std::string>("message") };
//        }
//    };

} // end namespace TestNode

#endif //BEHAVIORTREE_TEST_NODE_H






