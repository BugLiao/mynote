//
// Created by liao on 2021/8/8.
//

#include "test_node.h"


namespace TestNode
{
    //==============================SaySomething=============================//
    BT::NodeStatus SaySomething::tick()
    {
        auto msg = getInput<std::string>("message");
        if (!msg)
        {
            throw BT::RuntimeError( "missing required input [message]: ", msg.error() );
        }

        std::cout << "Robot says: " << msg.value() << std::endl;
        return BT::NodeStatus::SUCCESS;
    }


    //=================================Run===================================//
    BT::NodeStatus Run::tick()
    {
        std::cout << "Robot says: I'm run" << std::endl;
        return BT::NodeStatus::SUCCESS;
    }


    //=============================MoveAction================================//
    BT::NodeStatus MoveAction::tick()
    {
        velocity_2D vel;
        if ( !getInput<velocity_2D>("cmd_vel", vel))
        {
            throw BT::RuntimeError("missing required input [cmd_vel]");
        }

        printf("[ MoveBase: STARTED ]. goal: x=%.f y=%.1f theta=%.2f\n", vel.x, vel.y, vel.theta);

        _halt_requested.store(false);
        int count = 0;

        // 假设这里计算了1000ms
        // It is up to you to check periodicall _halt_requested and interrupt
        // this tick() if it is true.
        while (!_halt_requested && count++ < 10)
        {
            SleepMS(100);
        }

        std::cout << "[ MoveBase: FINISHED ]" << std::endl;
        return _halt_requested ? BT::NodeStatus::FAILURE : BT::NodeStatus::SUCCESS;
    }

    void MoveAction::halt()
    {
        _halt_requested.store(true);
    }

    //==============================SaySomething=============================//
//    BT::NodeStatus robot_state::tick()
//    {
//        auto msg = getInput<std::string>("message");
//        if (!msg)
//        {
//            throw BT::RuntimeError( "missing required input [message]: ", msg.error() );
//        }
//
//        if(msg == "run")
//        {
//            std::cout << "Robot is  " << msg.value() << std::endl;
//            return BT::NodeStatus::SUCCESS;
//        }
//
//        return BT::NodeStatus::FAILURE;
//    }


}
