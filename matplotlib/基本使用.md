## 创建画布

```python
import matplotlib.pyplot as plt
import numpy as np

# 设置画布和坐标簇
fig, ax = plt.subplots(2, 2, figsize=(10, 10))

# ax00  plot
x00 = np.linspace(0, 10, 100)
y00 = 4 + 2 * np.sin(2 * x00)
ax[0][0].plot(x00, y00, linewidth=2.0, color='r', label='LineA')
ax[0][0].plot(y00, x00, linewidth=2.0, color='b', label='LineB')
ax[0][0].legend()  # 线条说明

# ============ax01=============================
# scatter
x01 = 4 + np.random.normal(0, 2, 5)
y01 = 4 + np.random.normal(0, 2, 5)
colors = np.random.uniform(15, 80, 5)
ax[0][1].scatter(x01, y01, s=20, c=colors, vmin=0, vmax=100)

x01 = 4 + np.random.normal(0, 2, 5)
y01 = 4 + np.random.normal(0, 2, 5)
position = np.random.uniform(0, 100, (5, 2))
ax[0][1].scatter(position[:, 0], position[:, 1], s=50, c=colors, vmin=0, vmax=100)
# ============ax10=============================
# title
ax[1][0].set_title('Title', fontsize=18)
ax[1][0].set_xlabel('xlabel', fontsize=18, fontfamily='sans-serif', fontstyle='italic')
ax[1][0].set_ylabel('ylabel', fontsize='x-large', fontstyle='oblique')
# axis limit
ax[1][0].set_xlim(0, 12)
ax[1][0].set_ylim(0, 7)
# 坐标刻度
x_ticks = np.arange(0, 10, 1)
y_ticks = np.arange(0, 5, 0.5)
ax[1][0].set_xticks(x_ticks)
ax[1][0].set_yticks(y_ticks)
# 栅格
ax[1][0].grid(True)
# 设置背景颜色
ax[1][0].patch.set_facecolor('yellow')

# ============ax11=============================
img = plt.imread("map/rmua_2021_map.pgm")
ax[1][1].imshow(img, extent=[0, 173, 0, 102])
ax[1][1].scatter(10, 10, s=50, c="b", vmin=0, vmax=100)

plt.show()


```

