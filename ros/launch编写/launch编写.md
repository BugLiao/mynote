# roslaunch 文件编写
---
#  1、<node\>
**作用： 启动一个节点**
## 1.1 例子
```xml
<node name="listener1" pkg="rospy_tutorials" type="listener.py" args="--test" respawn="true" />
```
使用`rospy_tutorials`包中的`listener.py`可执行文件和命令行参数`--test`启动`listener1`节点。如果节点死掉，它将自动重启。

```xml
<node name="bar1" pkg="foo_pkg" type="bar" args="$(find baz_pkg)/resources/map.pgm" />
```
从`foo_pkg`包启动 `bar` 节点。这个例子使用替换参数传递一个可移植的引用到`baz_pkg/resources/map.pgm`。


## 1.2 Attributes（属性）
- **`pkg`** = `“mypackage”` - 节点所在的包
- **`type`** = `“nodetype”` - 节点类型。必须有一个对应的同名可执行文件。
- **`name`** = `“nodename”` - 节点名称。注意：名称不能包含命名空间。改用ns属性。
- **`args`** = `“arg1 arg2 arg3”`（可选） - 将参数传递给节点。
- **`machine`** = `“machine-name”`（可选） - 在指定机器上启动节点。[具体使用方法](http://wiki.ros.org/roslaunch/XML/machine)
- **`respawn`** = `“true”`（可选，默认：False） - 如果节点退出，则自动重新启动节点。
- **`respawn_delay`** = `“30”`（可选，默认：0） - 如果`respawn`为**true**，则在检测到节点故障后等待`respawn_delay`秒，然后再尝试重新启动。
- **`required`** = `“true”`（可选） -如果节点死亡，则杀死整个 roslaunch。
- **`ns`** = `foo`（可选） - 在命名空间`foo`中启动节点。
- **`clear_params`** = `"true|false"`（可选） - 在启动之前删除节点私有命名空间中的所有参数。
- **`output`** = `"log|screen"`（可选） - 如果是`"screen"`，则来自节点的 `stdout/stderr` 将被发送到屏幕。如果是`"log"`，`stdout/stderr` 输出将被发送到 `$ROS_HOME/log` 中的日志文件，并且 `stderr` 将继续发送到屏幕。默认值为`"log"`。
- **`cwd`** = `"ROS_HOME|node"`（可选） - 如果为`“node”`，则节点的工作目录将设置为与节点的可执行文件相同的目录。在 C Turtle 中，默认值为 `"ROS_HOME"`。在 Box Turtle (ROS 1.0.x) 中，默认值为`"ros-root"`。C Turtle 中不推荐使用`"ros-root"`。
- **`launch-prefix`** = `"prefix arguments"`（可选） - 附加到节点启动参数的命令/参数。这是一个强大的功能，使您能够启用gdb、valgrind、xterm、nice或其他方便的工具。[具体使用方法](http://wiki.ros.org/roslaunch/Tutorials/Roslaunch%20Nodes%20in%20Valgrind%20or%20GDB)


## 1.3 Elements （元素）
**可以在<node\>标签内使用以下XML标签：**
- ### **<env\>** - 为节点设置环境变量。
  **作用：** 该<ENV>标签k可以为**launch**文件中的**node**设置环境变量。此标签只能在<launch\>、<include\>、<node\>或<machine\>标签内使用。当它在<launch\>标签内使用时，<env>标签仅适用于在之后声明的节点。一些环境变量可以在**EnvironmentVariables**中找到。

  **注意:** 使用<env>标签设置的值不会被`$(env ...)`看到，因此<env>标签不能用于参数化**launch**文件。
  ##### Attributes (属性)
  - **`name`** = `“environment-variable-name”` - 正在设置的环境变量。
  - **`value`** = `“environment-variable-value”` - 将环境变量设置为。  
    

- ### **<remap\>** - 重映射。
  **作用：** 重新映射允许您“欺骗”一个 ROS 节点，以便当它认为它正在订阅或发布`topic A` 时，但它实际上是在订阅或发布`topic B`
  **注意:** `remap`在`node`之外的作用域是他之后的所有节点，在`node`中的作用域是当前节点.
  
  ##### Attributes (属性)
  - **`from`**=`"original-name"` - 原本要`发布|订阅`的主题
  - **`to=`** = `"new-name"` - 重映射的目标主题,实际上`发布|订阅`的主题  
  
  **demo1：订阅节点使用 remap**
  例如，有一个节点，它订阅了`“chatter”`话题，但现在只有一个`“hello”`话题在发布。这两个话题是同一类型，这时突然想那订阅`"hello"`也行，就当他是`"chatter"`算了。那就可以这样操作：
  ```xml
  #将别人发布的主题映射到自己订阅的主题上：
  <!--sub_node.launch-->
    <node pkg="sub_node" type="sub_node" name="sub_node" output="screen">
        <remap from="chatter" to="hello"/>
    </node>
  ```
  这个例子是从订阅主题的接收节点的角度来看的。因此，当该节点在其源代码中订阅主题`“chatter”`时，它会被重新映射为实际订阅`“hello”`，因此它最终会收到其他 ROS 节点发布到`“hello”`的所有消息。

  **demo2：发布节点使用 remap**
  重命名一个已经存在的主题：
  ```xml
    <!--pub_node.launch-->
    <node pkg="pub_node" type="pub_node" name="pub_node" output="screen">
        <remap from="pub_data" to="talker" />        
    </node> 
  ```
  这个例子是从主题的发布者的角度来看的。因此，该节点在其源代码中发布话题 `“pub_data”`时，它会被重新映射为发布`“talker”`。


- ### **<rosparam\>** - ROS参数。
  **作用：** `<rosparam>`标签用于从yaml文件中读取并设置参数。所述`<rosparam>`标签可以是放`<node>`标签的里面，在这种情况下，参数被视为私有名称。
  **注意:** `<rosparam>`标记可以引用**YAML**文件或包含原始**YAML**文本。 如果**YAML**文本定义为字典类型，则可以省略**param**属性。

  ##### Attributes (属性)
  - **`command`**=`"load|dump|delete"` (可选, default=load) - rosparam命令。
  - **`file`** = `"$(find pkg-name)/path/foo.yaml"`（load or dump commands） - rosparam 文件的名称。
  - **`param`**=`"param-name"` - 参数名称。
  - **`ns`**=`"namespace"` (可选) - 将参数范围限定到指定的命名空间。
  - **`subst_value`**=`true|false` (可选) - 允许在 YAML 文本中使用替换参数。
  
  **demo**
  ```XML
  <rosparam command="load" file="$(find rosparam)/example.yaml" />
  <rosparam command="delete" param="my/param" />
  <rosparam param="a_list">[1, 2, 3, 4]</rosparam>
  <rosparam>
    a: 1
    b: 2
  </rosparam>
  ```
  允许使用roslaunch里面的args表示部分或全部的**YAML**字符串。例如
  ```xml
  <arg name="whitelist" default="[3, 2]"/>
  <rosparam param="whitelist" subst_value="True">$(arg whitelist)</rosparam>
  ```
  在roscpp代码中访问列表的示例：
  ```cpp
  XmlRpc::XmlRpcValue v;
    nh_.param("subscribed_to_nodes", v, v);
    for(int i =0; i < v.size(); i++)
    {
      node_names_.push_back(v[i]);
      std::cerr << "node_names: " << node_names_[i] << std::endl;
    }
  ```

- ### **<param\>** - 参数。
  **作用：** `<param>`标签可用于直接设置参数，也可以从文件中读取。可以指定textfile、binfile或command属性来设置参数的值，而不是value。**所述<param\>是放在<node\>标签里面的，在这种情况下，参数被视为私有。**
  ##### Attributes (属性)
  - **`name`**=`"namespace/name"`  - 参数名称。命名空间可以包含在参数名称中，但应避免指定全局名称。
  - **`value`**=`"value"`（可选）  - 定义参数的值。如果省略此属性，则必须指定binfile、textfile或command。
  - **`type`**=`"str|int|double|bool|yamle"`（可选） - 指定参数的类型。如果不指定类型，roslaunch 将尝试自动确定类型。
    - 带有“.”的数字是浮点数，否则是整数；
    - “true”和“false”是布尔值（不区分大小写）。
    - 所有其他值都是字符串
  - **`textfile`**=`"$(find pkg-name)/path/file.txt"`（可选） - 文件的内容将被读取并存储为字符串。该文件必须可在本地访问，最好使用与包相关的`$(find)/file.txt`语法来指定位置。
  - **`binfile`**=`"$(find pkg-name)/path/file"`（可选） - 该文件的内容将被读取并存储为 base64 编码的 XML-RPC 二进制对象。该文件必须可在本地访问，最好使用与包相关的`$(find)/file.txt`语法来指定位置。
  - **`command`**=`"$(find pkg-name)/exe '$(find pkg-name)/arg.txt'"`（可选） - 命令的输出将被读取并存储为字符串。最好使用与包相关的`$(find)/file.txt`语法来指定文件参数。由于 XML 转义要求，还应该使用单引号引用文件参数。

  **demo**
  ```xml
  <param name="publish_frequency" type="double" value="10.0" />
  ```
  **从YAML 输出参数**
  要加载 YAML 文件，可以使用：
  ```xml
  <rosparam command="load" file="FILENAME" />
  ```
  但是无法在stdout上使用输出参数的命令。在这种情况下，可以使用新的参数类型yaml
  ```xml
  <param name="params_a" type="yaml" command="cat '$(find roslaunch)/test/params.yaml'" />
  ```  
  ---
# 2、<param\>

**作用：在参数服务器上设置参数**
**注意:**  与在<node\>里面使用的区别不大
## 2.1 Attributes （属性）
  - **`name`**=`"namespace/name"`  - 参数名称。命名空间可以包含在参数名称中，但应避免指定全局名称。
  - **`value`**=`"value"`（可选）  - 定义参数的值。如果省略此属性，则必须指定binfile、textfile或command。
  - **`type`**=`"str|int|double|bool|yamle"`（可选） - 指定参数的类型。如果不指定类型，roslaunch 将尝试自动确定类型。
    - 带有“.”的数字是浮点数，否则是整数；
    - “true”和“false”是布尔值（不区分大小写）。
    - 所有其他值都是字符串
  - **`textfile`**=`"$(find pkg-name)/path/file.txt"`（可选） - 文件的内容将被读取并存储为字符串。该文件必须可在本地访问，最好使用与包相关的`$(find)/file.txt`语法来指定位置。
  - **`binfile`**=`"$(find pkg-name)/path/file"`（可选） - 该文件的内容将被读取并存储为 base64 编码的 XML-RPC 二进制对象。该文件必须可在本地访问，最好使用与包相关的`$(find)/file.txt`语法来指定位置。
  - **`command`**=`"$(find pkg-name)/exe '$(find pkg-name)/arg.txt'"`（可选） - 命令的输出将被读取并存储为字符串。最好使用与包相关的`$(find)/file.txt`语法来指定文件参数。由于 XML 转义要求，还应该使用单引号引用文件参数。
  
## 2.2 Examples （例子）
```xml
<param name="publish_frequency" type="double" value="10.0" />
```
## 2.2 从YAML 输出参数
  要加载 YAML 文件，可以使用：
  ```xml
  <rosparam command="load" file="FILENAME" />
  ```
  但是无法在stdout上使用输出参数的命令。在这种情况下，可以使用新的参数类型yaml
  ```xml
  <param name="params_a" type="yaml" command="cat '$(find roslaunch)/test/params.yaml'" />
  ```  
---
# 3、<remap\>
**作用：重映射一个名称**
**注意:**  与上面在<node\>里面用法基本一样，目前没搞清楚在<node\>内外使用的区别。

---
# 4、<machine\>
**作用：声明是哪一台机器使用这个launch。**
**注意:**  用得比较少，懒得写直接看[官网](http://wiki.ros.org/roslaunch/XML/machine)

---


# 5、<rosparam\> 
**作用：`<rosparam>`标签用于从yaml文件中读取并设置参数**
**注意:** `<rosparam>`标记可以引用**YAML**文件或包含原始**YAML**文本。 如果**YAML**文本定义为字典类型，则可以省略**param**属性。与在`<node\>`里面使用区别不大。

  ## 5.1 Attributes (属性)
  - **`command`**=`"load|dump|delete"` (可选, default=load) - rosparam命令。
  - **`file`** = `"$(find pkg-name)/path/foo.yaml"`（load or dump commands） - rosparam 文件的名称。
  - **`param`**=`"param-name"` - 参数名称。
  - **`ns`**=`"namespace"` (可选) - 将参数范围限定到指定的命名空间。
  - **`subst_value`**=`true|false` (可选) - 允许在 YAML 文本中使用替换参数。
  
  ## 5.2 Examples （例子）
  ```XML
  <rosparam command="load" file="$(find rosparam)/example.yaml" />
  <rosparam command="delete" param="my/param" />
  <rosparam param="a_list">[1, 2, 3, 4]</rosparam>
  <rosparam>
    a: 1
    b: 2
  </rosparam>
  ```
  允许使用roslaunch里面的args表示部分或全部的**YAML**字符串。例如
  ```xml
  <arg name="whitelist" default="[3, 2]"/>
  <rosparam param="whitelist" subst_value="True">$(arg whitelist)</rosparam>
  ```
  在roscpp代码中访问列表的示例：
  ```cpp
  XmlRpc::XmlRpcValue v;
    nh_.param("subscribed_to_nodes", v, v);
    for(int i =0; i < v.size(); i++)
    {
      node_names_.push_back(v[i]);
      std::cerr << "node_names: " << node_names_[i] << std::endl;
    }
  ```
---

# 6、<include\>
**作用：包含其他roslaunch文件。类似C语言中的#include**
**注意:** `<include>`标签可以导入另一个**roslaunch**文件到当前文件。它将在当前文档的范围内导入，包括`<group>`和`<remap>`标签。包含文件中的所有内容都将被导入，除了`<master>`标签。

  ## 6.1 Attributes (属性)
  - **`file`**=`"$(find pkg-name)/path/filename.xml"`  - 要包含的文件名。
  - **`ns`**=`"namespace"` (可选) - 导入在“foo”命名空间的文件。
  - **`clear_params`**=`true|false` (可选, Default: false) - 在启动之前删除`<include>`命名空间中的所有参数。此功能非常危险，应谨慎使用。ns必须指定。默认值：false。
  - **`pass_all_args`**=`"true|false"`(可选, Default: false) - 如果为 `true`，则当前上下文中设置的所有参数都将添加到为处理包含的文件而创建的子上下文中。您可以这样做，而不是明确列出要传递的每个参数。

  ## 6.2 Elements (元素)
- ### **<env\>** - 在包含的整个文件中设置一个环境变量。
  #### Attributes
  - **`name`**=`"environment-variable-name"`  - 正在设置的环境变量。
  - **`value`**=`"environment-variable-value"`  - 将环境变量设置为的值。
  
- ### **<arg\>** - 将参数传递给包含的文件。
  **作用：** 给包含的文件传递参数，提高文件可重复使用性。
  **注意:** `Args`不是全局的。`arg` 声明特定于单个启动文件，很像方法中的本地参数。您必须显式地将`arg`值传递给包含的文件，就像您在方法调用中所做的那样。
    
  ##### 用例模式
  `<arg>`可以通过以下三种方式使用：
  ```xml
  <arg name="foo" />
  声明foo的存在。foo必须作为命令行参数或通过<include>来传递。

  <arg name="foo" default="1" />
  使用默认值声明foo。foo可以通过命令行参数或通过<include>覆盖。

  <arg name="foo" value="bar" />
  用常数值声明foo。foo的值不能被覆盖。这种用法可以实现启动文件的内部参数化，而无需在更高级别公开该参数化。
  ```
  ##### Attributes（属性）
  - **`name`**=`"arg_name"`  - 参数名称。
  - **`default`**=`"default value"`（可选） - 参数的默认值。不能与value属性结合使用。
  - **`value`**=`"value"`（可选） - 参数值。不能与默认属性结合使用。
  - **`doc`**=`"description for this arg"`（可选） - 论据的描述。您可以通过roslaunch命令的--ros-args参数获得此信息。
  
## 6.3 Examples
  ### 将参数传递给包含的文件
  `my_file.launch`:
  ```xml
  <include file="included.launch">
    <!-- all vars that included.launch requires must be set -->
    <arg name="hoge" value="fuga" />
  </include>
  ```
  `included.launch`:
  ```xml
  <launch>
    <!-- declare arg to be passed in -->
    <arg name="hoge" /> 
    <!-- read value of arg -->
    <param name="param" value="$(arg hoge)"/>
  </launch>
  ```
  ### 通过命令行传递参数
  roslaunch使用与 ROS 重映射参数相同的语法来指定arg值。
  ```bash
  roslaunch my_file.launch hoge:=my_value      (.launch file is available at the current dir)
  roslaunch %YOUR_ROS_PKG% my_file.launch hoge:=my_value
  ```
  ---
# 7、<env\>
**作用：为启动的节点指定环境变量**
**注意:** 此标签只能在`<launch>`、`<include>`、`<node>`或`<machine>`标签的范围内使用。当它在`<launch>`标签内使用时，`<env>`标签仅适用于在节点声明之后。一些环境变量可以在`EnvironmentVariables`中找到。
## Attributes
- **`name`**=`"environment-variable-name"`  - 正在设置的环境变量。
- **`value`**=`"environment-variable-value"`  - 将环境变量设置为的值。
---
# 8、<test\>
**作用：启动一个测试节点**
暂时用不上，用上了再学

---
# 9、<arg\>
**作用：参数，类似代码里面的变量**
**注意:** 和上面include里面的差不多
    
  ##### 用例模式
  `<arg>`可以通过以下三种方式使用：
  ```xml
  <arg name="foo" />
  声明foo的存在。foo必须作为命令行参数或通过<include>来传递。

  <arg name="foo" default="1" />
  使用默认值声明foo。foo可以通过命令行参数或通过<include>覆盖。

  <arg name="foo" value="bar" />
  用常数值声明foo。foo的值不能被覆盖。这种用法可以实现启动文件的内部参数化，而无需在更高级别公开该参数化。
  ```
  ## 9.1 Attributes（属性）
  - **`name`**=`"arg_name"`  - 参数名称。
  - **`default`**=`"default value"`（可选） - 参数的默认值。不能与value属性结合使用。
  - **`value`**=`"value"`（可选） - 参数值。不能与默认属性结合使用。
  - **`doc`**=`"description for this arg"`（可选） - 论据的描述。您可以通过roslaunch命令的--ros-args参数获得此信息。
  
## 9.2 Examples
  ### 将参数传递给包含的文件
  `my_file.launch`:
  ```xml
  <include file="included.launch">
    <!-- all vars that included.launch requires must be set -->
    <arg name="hoge" value="fuga" />
  </include>
  ```
  `included.launch`:
  ```xml
  <launch>
    <!-- declare arg to be passed in -->
    <arg name="hoge" /> 
    <!-- read value of arg -->
    <param name="param" value="$(arg hoge)"/>
  </launch>
  ```
  ### 通过命令行传递参数
  roslaunch使用与 ROS 重映射参数相同的语法来指定arg值。
  ```bash
  roslaunch my_file.launch hoge:=my_value      (.launch file is available at the current dir)
  roslaunch %YOUR_ROS_PKG% my_file.launch hoge:=my_value
  ```
---
# 10、<group\>
**作用：对共享命名空间或重新映射的封闭元素进行分组**
**注意:** 所述`<group>`标记使得更容易设置应用到一组节点。它有一个`ns`属性，可让您将节点组推送到单独的命名空间中。您还可以使用<remap>标签在整个组中应用重新映射设置

## 10.1 Attributes（属性）
  - **`ns`**=`"namespace"`（可选）  - 将节点组分配给指定的命名空间。命名空间可以是全局的或相对的，但不鼓励使用全局命名空间。
  - **`clear_params`**=`"default value"`（可选） - 启动前删除组命名空间中的所有参数。此功能非常危险，应谨慎使用。ns必须指定。

## 10.2 Elements(元素)
  - **`<node>`** - 启动一个节点
  - **`<param>`** - 在参数服务器上设置参数
  - **`<remap>`** - 声明一个名称重新映射。
  - **`<machine>`** - 声明一台用于启动的机器。
  - **`<rosparam>`** -使用rosparam文件为启动设置 ROS 参数。
  - **`<include>`** -包括其他roslaunch文件。
  - **`<env>`** -为启动的节点指定环境变量。
  - **`<test>`** -启动一个测试节点（见rostest）。
  - **`<arg>`** 声明一个参数。

  