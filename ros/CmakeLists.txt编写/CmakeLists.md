
# CmakeLists 整体结构和排序

[toc]
---
##  0、CMakeLists.txt 文件必须遵循此格式，否则包将无法正确构建。配置中的顺序很重要。

1. 所需的 CMake 版本 (cmake_minimum_required)

2. 包名 (project())

3. 查找构建所需的其他 CMake/Catkin 包 (find_package())

4. 启用 Python 模块支持 (catkin_python_setup())

5. 消息/服务/动作生成器(add_message_files(), add_service_files(), add_action_files())

6. 调用消息/服务/操作生成 (generate_messages())

7. 指定包构建信息导出 (catkin_package())

8. 要构建的库/可执行文件(add_library()/add_executable()/target_link_libraries())

9. 要构建的测试 (catkin_add_gtest())

10. 安装规则(install())

---
## 1、cmake_minimum_required(VERSION 2.8.3)
定义CMake 版本 (固定要写)  
每个 catkin CMakeLists.txt 文件都必须以所需的 CMake 版本开头。Catkin 需要 2.8.3 或更高版本。
```bash
cmake_minimum_required(VERSION 2.8.3)
```
---
## 2、project()
CMake项目函数指定包的名称(固定要写)。比如我们正在制作一个名为robots_brain的包
```bash
project(robot_brain)
```
在 CMake 中，可以再CMake 脚本中任何需要的地方使用变量${PROJECT_NAME }引用项目名称。

---
## 3、find_package()
查找构建本包时依赖的其他cmake包 
```bash
find_package(catkin REQUIRED)
```
如果项目依赖其他的wet包，它们会自动变成catkin的组件（就CMake而言）。如果将它们指定为组件，而不是在这些包上使用find_package，它将更简单。例如，如果使用包nodelet
```bash
find_package(catkin REQUIRED COMPONENTS nodelet)
```
### 3.1、find_package() 有什么作用  
如果 CMake 通过 find_package 找到一个包，则会导致创建多个 CMake 环境变量，提供有关找到的包的信息。这些环境变量可以稍后在 CMake 脚本中使用。环境变量描述了包导出头文件的位置、源文件的位置、包依赖的库以及这些库的路径。名称始终遵循 <PACKAGE NAME\>_<PROPERTY\> 的约定：

- <NAME\>_FOUND - 如果找到库，则设置为 true，否则为 false
- <NAME\>_INCLUDE_DIRS 或 <NAME\>_INCLUDES - 包导出的包含路径
- <NAME\>_LIBRARIES 或 <NAME\>_LIBS - 包导出的库
- <NAME\>_DEFINITIONS - ?  

### 3.2、 为什么将 Catkin 包指定为组件？  
Catkin 包并不是真正的 catkin 组件,它这么写主要是为了节省大量的打字时间。
对于 catkin 包，如果您 find_package 它们作为 catkin 的组件，这是非常好用的，因为使用catkin_前缀创建了一组环境变量。例如，假设您在代码中使用了nodelet包。查找包的推荐方法是：
```bash
find_package(catkin REQUIRED COMPONENTS nodelet)
```
这意味着 nodelet 导出的包含路径、库等也附加到 catkin_ 变量。  
例如，catkin_INCLUDE_DIRS 不仅包含 catkin 的包含路径，还包含 nodelet 的包含路径！这将在以后派上用场。

如果使用
```bash
find_package(nodelet)
```
则意味着 nodelet 路径、库等不会添加到 catkin_ 变量中。  

这会导致 nodelet_INCLUDE_DIRS、nodelet_LIBRARIES 等也使用创建相同的变量

### 3.3、 Boost
如果使用 C++ 和 Boost，需要在 Boost 上调用find_package()并指定您使用 Boost 的哪些部分作为组件。  

例如，如果想使用 Boost 线程，便这样写：
```bash
find_package(Boost REQUIRED COMPONENTS thread)
```
---
## 4、catkin_package()
catkin_package()是作用在find_package()中的。  

它安装 package.xml 文件，并为find_package和pkg-config生成代码， 以便其他包可以获取有关此包的信息。  
  
必须在使用add_library()或add_executable()声明任何目标之前调用此函数。该函数有 5 个可选参数：
- INCLUDE_DIRS (list of strings) : CMAKE_CURRENT_SOURCE_DIR - C/C++ 的相对路径包括
- LIBRARIES(list of strings) : 从项目导出的库 如果逻辑目标名称与安装的名称不同，则会中断此操作。
- CATKIN_DEPENDS(list of strings) : DEPENDS以及CATKIN_DEPEND 告诉 catkin 您的包的哪些依赖项应该传递给find_package  

- DEPENDS(list of strings) : 该项目所依赖的 CMake 项目列表。由于它们可能不是find_packagable或缺少 pkg-config 文件，因此它们INCLUDE_DIRS和LIBRARIES被直接传递。这要求它之前已经被find_package -ed。
- CFG_EXTRAS(strings) : 附加配置选项  

举个例子
```txt
catkin_package(
   INCLUDE_DIRS include
   LIBRARIES ${PROJECT_NAME}
   CATKIN_DEPENDS roscpp nodelet
   DEPENDS eigen opencv)
```
这表明包文件夹中的“include”文件夹是导出头文件的位置。CMake 环境变量 ${PROJECT_NAME} 是之前传递给project()函数的内容，在上面例子中，它将是“robot_brain”。“roscpp”+“nodelet”是构建和运行这个包需要依赖的包，而“eigen”+“opencv”是构建/运行这个包需要的系统依赖项。

---
## 5、指定构建目标

构建目标可以采用多种形式，但通常它们代表两种可能性之一：

- Executable Target（） - 我们可以运行的程序
- Library Target（） - 可执行目标在构建和/或运行时可以使用的库

demo:

```
add_executable(foo src/foo.cpp)
add_library(moo src/moo.cpp)
target_link_libraries(foo moo)  -- This links foo against libmoo.so
```
### 5.1 目标命名
需要注意的是，无论构建/安装到哪个文件夹，catkin 中构建目标的名称都必须是唯一的。这是 CMake 的要求。但是，目标的唯一名称仅在 CMake 内部是必需的。可以使用set_target_properties()函数将目标重命名为其他目标：
```bash
set_target_properties(rviz_image_view
                      PROPERTIES OUTPUT_NAME image_view
                      PREFIX "")
```
### 5.2 自定义输出目录
虽然可执行文件和库的默认输出目录通常设置为合理的值，但在某些情况下必须对其进行自定义。即包含 Python 绑定的库必须放在不同的文件夹中才能在 Python 中导入
```txt
set_target_properties(python_module_library
  PROPERTIES LIBRARY_OUTPUT_DIRECTORY ${CATKIN_DEVEL_PREFIX}/${CATKIN_PACKAGE_PYTHON_DESTINATION})
```

### 5.3 包括路径和库路径
在指定目标之前，需要指定可以在何处找到这些目标的资源，特别是头文件和库：

- Include Paths  - 在哪里可以找到正在构建的代码（在 C/C++ 中最常见）的头文件
- Library Paths - 可执行目标构建所针对的库位于何处？
- include_directories(<dir1\>, <dir2\>, ..., <dirN\>)
- link_directories(<dir1\>, <dir2\>, ..., <dirN\>)

include_directories 的参数应该是由 find_package 调用生成的 *_INCLUDE_DIRS 变量以及需要包含的任何其他目录。如果使用 catkin 和 Boost，则 include_directories() 调用应如下所示：

```
include_directories(include ${Boost_INCLUDE_DIRS} ${catkin_INCLUDE_DIRS})
```
CMake link_directories()函数可用于添加额外的库路径，但不建议这样做。所有 catkin 和 CMake 包在 find_packaged 时都会自动添加链接信息。只需链接到target_link_libraries()中的库
```
link_directories(~/my_libs)
```

### 5.4 可执行目标

这将构建一个名为myProgram的目标可执行文件，它由 3 个源文件构建：src/main.cpp、src/some_file.cpp 和 src/another_file.cpp。

```
add_executable(myProgram src/main.cpp src/some_file.cpp src/another_file.cpp)
```

### 5.4 构建库目标
该add_library（） CMake的功能是用来指定库来构建。默认情况下，catkin 构建共享库。
```
add_library(${PROJECT_NAME} ${${PROJECT_NAME}_SRCS})
```

### 5.5 target_link_libraries()
使用target_link_libraries()函数指定可执行目标链接到哪些库。这通常在add_executable()调用之后完成。如果没有找到 ros ，则添加${catkin_LIBRARIES}。

```
target_link_libraries(<executableTargetName>, <lib1>, <lib2>, ... <libN>)
```
在大多数用例中不需要使用link_directories()，因为该信息是通过find_package()自动获取的。

---
## 6、启动python模块
如果 ROS 包提供了一些 Python 模块，你应该创建一个setup.py文件并调用
```
catkin_python_setup()

```