# ros 服务的编写
参考：
- http://wiki.ros.org/roscpp/Overview/Services

**demo任务描述：** 客户端（client）发送两个int值到服务端，服务端(service)将这两个值相加，结果返回给客户端

---

## 1、创建一个rospackage测试ros server
```bash
catkin create pkg ros_server_demo --catkin-deps roscpp
```
---
## 2、创建srv
1. 在`ros_server_demo`包下创建srv文件夹
2. 在srv文件夹下新建文件`addTwoInts.srv`
    ```service
    int64 a
    int64 b
    ---
    int64 sum
    ```
3. 在**package.xml**写一下构建依赖
    ```xml
    <build_depend>message_generation<build_depend>
    <exec_depend>message_runtime</exec_depend>
    ```
4. 在**CMakeLists.txt**修改以下部分
    ```txt
    find_package(catkin REQUIRED COMPONENTS
        roscpp
        std_msgs
        message_generation
    )
    ```

    ```txt
    add_service_files(
        FILES
        AddTwoInts.srv
    )
    ```

    ```txt
    generate_messages(
    DEPENDENCIES
    std_msgs  # Or other packages containing msgs
    )
    ```

5. 编译这个rospackage,生成srv头文件
   ```bash
   cd ~/catkin_ws
   catkin build ros_server_demo 
   ```
   这时候在`~/catkin_ws/devel/include/ros_server_demo`路径下会生成以下文件，这是在后面调用服务的时候会用到的
   ![pic](pic/01.png)

   现在可以尝试查找这个服务
   ```bash
   cd ~/catkin_ws
   source devel/setup.bash 
   rossrv show ros_server_demo/addTwoInts
   ```
   会显示出
   ```bash
    int64 a
    int64 b
    ---
    int64 sum
    ```
这时srv文件构建成功

---
## 3、编写客户端（client）和服务器端(service)
#### 1. 编写client节点

在`src`文件夹新建文件`add_two_ints_client.cpp`
```cpp
#include "ros/ros.h"
#include "ros_server_demo/addTwoInts.h"
#include <cstdlib>

int main(int argc, char **argv)
{
    ros::init(argc, argv, "add_two_ints_client");
    if (argc != 3)
    {
        ROS_INFO("usage: add_two_ints_client X Y");
        return 1;
    }

    ros::NodeHandle n;
    ros::ServiceClient client = n.serviceClient<ros_server_demo::addTwoInts>("add_two_ints");
    ros_server_demo::addTwoInts srv;
    srv.request.a = atoll(argv[1]);
    srv.request.b = atoll(argv[2]);

    if (client.call(srv))
    {
        ROS_INFO("Sum: %ld", (long int)srv.response.sum);
    }
    else
    {
        ROS_ERROR("Failed to call service add_two_ints");
        return 1;
    }

    return 0;
}
```
#### 2. 编写service节点
在`src`文件夹新建文件`add_two_ints_server.cpp`
```cpp
#include "ros/ros.h"
#include "ros_server_demo/addTwoInts.h"

bool add(ros_server_demo::addTwoInts::Request  &req,
         ros_server_demo::addTwoInts::Response &res)
{
    res.sum = req.a + req.b;
    ROS_INFO("request: a = %ld , b = %ld", (long int)req.a, (long int)req.b);
    ROS_INFO("sending back response: [%ld]", (long int)res.sum);
    return true;
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "add_two_ints_server");
    ros::NodeHandle n;

    ros::ServiceServer service = n.advertiseService("add_two_ints", add);
    ROS_INFO("Ready to add two ints.");
    ros::spin();

    return 0;
}
```
#### 3. 修改CMakeList.txt文件，用以生成可执行文件
在CMakeList.txt最后添加
```txt
add_executable(add_two_ints_server src/add_two_ints_server.cpp)
target_link_libraries(add_two_ints_server ${catkin_LIBRARIES})
add_dependencies(add_two_ints_server ros_server_demo_gencpp)

add_executable(add_two_ints_client src/add_two_ints_client.cpp)
target_link_libraries(add_two_ints_client ${catkin_LIBRARIES})
add_dependencies(add_two_ints_client ros_server_demo_gencpp)
```


#### 4. 编译节点
```bash
cd ~/catkin_ws
catkin build ros_server_demo
```


---
## 4、测试服务
- 命令行窗口1
  ```bash
  rosrun ros_server_demo add_two_ints_server
  ```
- 命令行窗口2
  ```bash
  rosrun ros_server_demo add_two_ints_client 1 2
  ```
  ![pic](pic/02.png)