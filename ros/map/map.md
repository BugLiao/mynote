## map文件说明:一个map包含两个文件`map.pgm`和`map.yaml`
### map.yaml
```xml
image: testmap.png        # 图片路径
resolution: 0.1           # 地图分辨率，米/像素
origin: [0.0, 0.0, 0.0]   # 左下角像素的坐标（x，y，yaw）
occupied_thresh: 0.65     # 占有概率大于这个阈值为占用
free_thresh: 0.196        # 占有概率小于这个阈值为自由
negate: 0                 # 是否转换占用和自由
```

- **`image`**:指定包含occupancy data的image文件路径; 可以是绝对路径，也可以是相对于YAML文件的对象路径 。
- **`resolution`**:地图分辨率，单位是meters/pixel 。
- **`origin`**:图中左下角像素的二维位姿，如（x，y，yaw），yaw逆时针旋转(yaw=0表示没有旋转)。系统的很多部分现在忽略yaw值。
- **`occupied_thresh`**:像素占用率大于这个阈值则认为完全占用。
- **`free_thresh`**:像素占用率比该阈值小被则认为完全自由。占用被表示为范围[0,100]中的整数，0意味着完全自由，100意味着完全被占用，并且特殊值-1用于完全未知。

- **`negate`** : 白/黑自由/占据语义是否应该反转（阈值的解释不受影响）
### map.pgm

就一张图片
***
## map服务器

### 加载地图 map_server
- map_server是一个 ROS 节点，它从磁盘读取地图并将地图提供给通过 ROS 服务。
- map_server 的当前实现将地图图像数据中的颜色值转换为三元占用值：空闲 (0)、已占用 (100) 和未知 (-1)。

**example**
```bash
rosrun map_server map_server rmua_map.yaml
```
**Parameters**
`~frame_id`(string, default: "map") - 设置要发布的地图的名称。


### 保存地图 map_saver
map_saver将地图保存到磁盘
```bash
rosrun map_server map_saver -f ~/catkin_ws/src/demo/diff_2_wheel/map/rmua_map
```

