# ros action demo

参考：
[actionlib](http://wiki.ros.org/cn/actionlib)
[actionlib/Tutorials](http://wiki.ros.org/actionlib/Tutorials)

功能描述：一个开枪的action，client发出开几枪的请求，服务端开枪，返回状态和结果

---
## 1、创建一个action测试包 
```bash
catkin create pkg ros_action_demo --catkin-deps actionlib message_generation roscpp std_msgs actionlib_msgs
```
---
## 2、定义生成action相关文件
#### 1.创建action文件`Shoot.action`
```bash
# Define the goal
int32 Number_of_shots  # 一共打几枪
---
# Define the result
string finish  # 打完没
---
# Define a feedback message
int32 Which_shot  # 开到第几枪了
```

#### 2. 修改cmakelist.txt这三部分
```xml
find_package(catkin REQUIRED COMPONENTS
        actionlib
        actionlib_msgs
        message_generation
        roscpp
        std_msgs
        genmsg
)

add_action_files(
        DIRECTORY action
        FILES DoDishes.action
)

generate_messages(
        DEPENDENCIES actionlib_msgs#   std_msgs
)

```

#### 3. 修改package.xml
**注意：** 如果创建rospkg时有添加actionlib和actionlib_msgs项，则无需此步
在末尾添加
```xml
<depend>actionlib</depend>
<depend>actionlib_msgs</depend>
```

#### 4. 生成该action相关的数据包
```bash
cd ~/catkin_ws
catkin build ros_action_demo
```
在`~/catkin_ws/devel/include/ros_action_demo`路径下生成以下文件
![pic](pic/02.png)

---

## 3、 编写action的客户端（client）和服务端(server)
### 客户端（client）
创建`shoot_client.cpp`
```cpp
#include <ros_action_demo/ShootAction.h> // Note: "Action" is appended
#include <actionlib/client/simple_action_client.h>

typedef actionlib::SimpleActionClient<ros_action_demo::ShootAction> Client;

int main(int argc, char** argv)
{
    ros::init(argc, argv, "shoot_client");
    Client client("shoot", true); // true -> don't need ros::spin()

    ROS_INFO("Waiting for action server to start.");
    client.waitForServer();
    ROS_INFO("Action server started, sending goal.");

    ros_action_demo::ShootGoal goal;
    goal.Number_of_shots = 10;
    client.sendGoal(goal);

    bool finished_before_timeout = client.waitForResult(ros::Duration(10.0));
    if (finished_before_timeout)
    {
        actionlib::SimpleClientGoalState state = client.getState();
        ROS_INFO("Action finished: %s",state.toString().c_str());
    }else{
        ROS_INFO("Action did not finish before the time out.");
    }
    return 0;
}
```

### 服务端（server）
创建`shoot_server.cpp`
```cpp
#include <ros_action_demo/ShootAction.h>  // Note: "Action" is appended
#include <actionlib/server/simple_action_server.h>

typedef actionlib::SimpleActionServer<ros_action_demo::ShootAction> Server;

void execute(const ros_action_demo::ShootGoalConstPtr& goal, Server* server)
{
    ros_action_demo::ShootFeedback feedback;
    ros_action_demo::ShootResult result;

    ros::Rate r(2); // 一秒开两枪
    bool success = true;


    for(int i=1; i <= 5; i++)
    {
        if (server->isPreemptRequested() || !ros::ok())
        {
            ROS_INFO("shoot is Preempted");
            success = false;
            break;
        }

        ROS_INFO("the NO.%d shoot ", i);
        feedback.Which_shot = i;
        server->publishFeedback(feedback);
        r.sleep();
    }

    if(success)
    {
        result.finish = "yes";
        server->setSucceeded(result);
    }

}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "shoot_server");
    ros::NodeHandle n;
    Server server(n, "shoot", boost::bind(&execute, _1, &server), false);
    server.start();
    ros::spin();
    return 0;
}
```

### 生成节点
#### 1.  修改cmakelist.txt，在末尾添加
```txt
add_executable(shoot_client src/shoot_client.cpp)

target_link_libraries(
        shoot_client
        ${catkin_LIBRARIES}
)

add_dependencies(
        shoot_client
        ${ros_action_demo_EXPORTED_TARGETS}
)


add_executable(shoot_server src/shoot_server.cpp)

target_link_libraries(
        shoot_server
        ${catkin_LIBRARIES}
)

add_dependencies(
        shoot_server
        ${ros_action_demo_EXPORTED_TARGETS}
)
```
#### 2. 编译
```bash
cd ~/catkin_ws
catkin build ros_action_demo
```

---

## 4、测试
```bash
rostopic echo /shoot/feedback
rostopic echo /shoot/result
rosrun ros_action_demo shoot_server
rosrun ros_action_demo shoot_client
```
![pic](pic/03.png)