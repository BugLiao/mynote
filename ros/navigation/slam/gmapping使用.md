## gmapping launch配置
[API接口](http://wiki.ros.org/gmapping)
```xml
<launch>
    <node pkg="gmapping" type="slam_gmapping" name="slam_gmapping" output="screen">
    
        <!-- 每隔throttle_scans （默认值 1）帧数据计算一次，限流作用 -->
        <param name="throttle_scans" value="1"/>

        <!-- 使用到的坐标名称 -->
        <param name="base_frame" value="base_link"/>
        <param name="odom_frame" value="odom"/>
        <param name="map_frame" value="map"/>

        <!-- 地图更新的时间（以秒为单位）。数字越小更新越快，但代价是计算量也跟着增加。 -->
        <param name="map_update_interval" value="1.0"/>
        <!-- 雷达数据的最大可用距离范围。 -->
        <param name="maxUrange" value="16.0"/> 
        <!-- 贪婪端点匹配使用的 sigma -->
        <param name="sigma" value="0.05"/>
        <!-- 在其中寻找对应的内核 -->
        <param name="kernelSize" value="1"/>
        <!-- 平移的优化步长 -->
        <param name="lstep" value="0.05"/>
        <!-- 旋转的优化步长 -->
        <param name="astep" value="0.05"/>
        <!-- 雷达信号匹配的迭代次数 -->
        <param name="iterations" value="5"/>
        <!-- 用于似然计算的Sigma -->
        <param name="lsigma" value="0.075"/>
        <!-- 在评估可能性时使用的增益，用于平滑重采样效果 -->
        <param name="ogain" value="3.0"/>
        <!-- 每次扫描要跳过的光束数。只取第(n+1)条激光以计算匹配(0 =取所有射线) -->
        <param name="lskip" value="0"/>

        <!-- 作为平移函数的平移里程计误差 (rho/rho) -->
        <param name="srr" value="0.1"/>
        <!-- 作为旋转函数的平移里程计误差 (rho/theta) -->
        <param name="srt" value="0.2"/>
        <!-- 作为平移函数的旋转里程计误差 (theta/rho) -->
        <param name="str" value="0.1"/>
        <!-- 作为旋转函数的旋转里程计误差 (theta/theta) -->
        <param name="stt" value="0.2"/>

        <!-- 每次机器人平移这么远时进行一次扫描 -->
        <param name="linearUpdate" value="1.0"/>
        <!-- 每次机器人旋转这么远时都会进行一次扫描 -->
        <param name="angularUpdate" value="0.5"/>
        <!-- 如果最后一次处理scan数据的时间比更新时间(以秒为单位)要早，则马上处理scan数据。如果value小于0的则关闭基于时间的更新。 -->
        <param name="temporalUpdate" value="3.0"/>

        <!-- 基于 Neff 的重采样阈值 -->
        <param name="resampleThreshold" value="0.5"/>
        <!-- 过滤器中的颗粒数 -->
        <param name="particles" value="30"/>

        <!-- 初始地图大小（以米为单位） -->
        <param name="xmin" value="-50.0"/>
        <param name="ymin" value="-50.0"/>
        <param name="xmax" value="50.0"/>
        <param name="ymax" value="50.0"/>

        <!-- 地图的分辨率（单位：每个网格块边长N米） -->
        <param name="delta" value="0.05"/>

        <!-- 似然的平移采样范围 -->
        <param name="llsamplerange" value="0.01"/>
        <!-- 似然的平移采样步长 -->
        <param name="llsamplestep" value="0.01"/>
        <!-- 似然的角度采样范围 -->
        <param name="lasamplerange" value="0.005"/>
        <!-- 似然的角度采样步长 -->
        <param name="lasamplestep" value="0.005"/>

        <!-- TF发布之间的时长（以秒为单位）。要禁用广播TF，请设置为0。 -->
        <param name="transform_publish_period" value="0.05"/>
        <!-- gmapping 占用值的阈值。占用率较高的单元格被视为已占用（即，在结果sensor_msgs/LaserScan 中设置为 100 ）。 -->
        <param name="transform_publish_period" value="0.05"/>
        <!-- 传感器的最大范围。如果传感器范围内没有障碍物的区域应在地图中显示为可用空间，请设置 maxUrange < 实际传感器的最大范围 <= maxRange。 -->
        <param name="transform_publish_period" value="0.05"/>
    </node>

</launch>
```
## 保存地图
```bash
rosrun map_server map_saver -f ~/catkin_ws/src/demo/diff_2_wheel/map/rmua_map
```
