## 安装工作区内所有ros包的依赖项
```bash
rosdep install --from-paths src --ignore-src -r -y
```

## 为某个包安装依赖项
```bash
rosdep install AMAZING_PACKAGE
```