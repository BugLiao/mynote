# 多台机器人连接到一个master，以实现多机通信

例子:  服务器 server 运行 master, 机器人rapper 连接到server的master

## 1、获取所有设备的IP
假设

`server IP` - 192.168.3.2  
`rapper IP` - 192.168.3.3

## 2、修改server 的 `.bashrc` 文件

在`.bashrc` 文件后面添加
```
export ROS_MASTER_URI=http://192.168.3.2:11311
export ROS_IP=192.168.3.2
```



## 3、修改rapper 的 `.bashrc` 文件
在`.bashrc` 文件后面添加

```
export ROS_MASTER_URI=http://192.168.3.2:11311
export ROS_IP=192.168.3.3
```


## 4、修改好后，记得source 一下 `.bashrc`

```bash
source ~/.bashrc
```
