# 原理（指针）
HEAD（当前使用分支） -> 分支 -> 版本

# 命令
`git add` - 追踪文件, 将修改添加到暂存区 

` git restore --staged readme.md ` - 取消暂存，取消上面一步的操作

`git status` - 查看当前状态，在哪个分支

`git reflog` - 查看当前分支的版本信息

` git reset --hard 8c0f4fb` - 版本穿梭，也就是切换版本，版本与commit有关

## 分支

`git branch dev` - 创建分支dev

`git checkout dev` - 切换分支dev

`git merge dev` - 合并分支（正常）



 