# 平衡车建模
文档撰写：廖伦稼
日期：2022/08/16
## 参数符号

| 名字           | 意义               | 值        |
|:-------------:|:------------------:|:--------:|
| M             | 车体质量            |2.0 kg|
| m             | 轮子质量            |0.23 kg|
| L             | 车体质心到车轴中心的距离 |0.2 m|
| $I_\omega$    | 车轮旋转惯量 $\frac{1}{2}mR^2$        |0.000414 $kg.m^2$|
| $I_\theta$    | 车体绕 pitch 轴的旋转惯量$\frac{1}{3}ML^2$ |0.026666 $kg.m^2$|
| $I_\delta$    | 车体绕 yaw 轴的旋转惯量 $\frac{1}{12}M（a^2+b^2)$|0.033333 $kg.m^2$|
| R             | 车轮半径 |0.06 m   |
| D             | 轮距 |0.432 m|
| g             | 重力加速度 |$9.81m/s^2$|
| $\theta$      | 车体与 Z 轴的夹角（倾角）pitch|
| $\delta$      | 车体与 X 轴的夹角 (偏角) yaw|
| $C_l , C_r$   | 左右轮输出力矩 ||
| $x_l , x_r, x, x_p$   | 左轮，右轮，轴心，车体在X轴位移（轴心坐标系） ||
| $z_p$   |车体在Z轴位移 （轴心坐标系）||
| $f_l , f_r$   | 左右轮与地面的摩擦力 ||
| $H_l , H_r$   | 左右轮在水平方向给车体的力 ||
| $V_l , V_r$   | 左右轮在垂平方向给车体的力 ||


<div align=center>
  <img src="doc/01.png" width="60%" />
  <img src="doc/02.png" width="60%" />
  <img src="doc/03.png" width="60%" />
</div>

---

# 1、建模
## 对左轮进行动力学分析：
$$
m \cdot \ddot{x_l} =  f_l - H_l \tag{1.1} 
$$

$$
I_\omega \cdot \frac{\ddot{x_l}}{R} = C_l - f_l \cdot R \tag{1.2}
$$

联立（1）、（2）得:
$$
\ddot{x_l} = \frac{C_lR -  H_lR^2}{I_\omega + mR^2} \tag{1.3}
$$


同理得右轮的方程：
$$
\ddot{x_r} = \frac{C_rR -  H_rR^2}{I_\omega + mR^2} \tag{1.4}
$$


联立（1.1）（1.2）（1.3）（1.4）得：
$$
\begin{split}
H_l + H_r &= \frac{1}{R}（C_l + C_r）- \frac{I_\omega + R^2}{R^2}（\ddot{x_l} + \ddot{x_r}）\\
&= \frac{1}{R}（C_l + C_r）- \frac{I_\omega + mR^2}{R^2} \cdot 2 \cdot\ddot{x}
\tag{1.5}
\end{split}
$$
## 对车身进行动力学分析：

在$x$方向的力：
$$
M \cdot \ddot{x_p} = H_l + H_r \tag{1.6}
$$

在$pitch$轴向的合外力矩：
$$
I_\theta \cdot \ddot{\theta} = ( V_l +  V_r) \cdot L \cdot \sin \theta - ( H_l +  H_r) \cdot L \cdot \cos \theta - (C_l + C_r)   \tag{1.7}
$$

$$
M \cdot \ddot{z_p} = V_l +  V_r - M \cdot g  \tag{1.8}
$$

在$yaw$轴向的合外力矩：
$$
I_\delta \cdot \ddot{\delta} = \frac{D}{2} \cdot ( H_r -  H_l) \tag{1.9}
$$

## 运动学分析 ：
轴心运动学分析：

$$
x = \frac{1}{2}(x_l + x_r) \tag{1.10} 
$$

$$
\dot{x} = \frac{1}{2}(\dot{x_l} + \dot{x_r}) \tag{1.11} 
$$

$$
\ddot{x} = \frac{1}{2}(\ddot{x_l} + \ddot{x_r})  \tag{1.12} 
$$

车体运动学分析：
$$
x_p = x + L \sin \theta   \tag{1.13} 
$$


$$
\dot{x_p} =  \dot{x}  + L \dot{\theta}  \cos \theta  \tag{1.14} 
$$

$$
\ddot{x_p}  =  \ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta \tag{1.15} 
$$

$$
z_p = L  \cos \theta \tag{1.16} 
$$

$$
\dot{z_p} = - L \dot{\theta}  \sin \theta \tag{1.17} 
$$ 

$$
\ddot{z_p} = - L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta \tag{1.18} 
$$

$$
\dot{\delta} = (\dot{x_r} - \dot{x_l}) \cdot \frac{1}{D} \tag{1.19} 
$$

$$
\ddot{\delta} = (\ddot{x_r} - \ddot{x_l}) \cdot \frac{1}{D} \tag{1.20} 
$$

---

# 2、获取  $\ddot{x}$ 与状态的关系

由 （1.12）得：

$$
\begin{split}
\ddot{x} &= \frac{1}{2}(\ddot{x_l} + \ddot{x_r})\\
代入（1.3）、（1.4）:&=\frac{1}{2}(\frac{C_lR -  H_lR^2}{I_\omega + mR^2} + \frac{C_rR -  H_rR^2}{I_\omega + mR^2}) \\
&=\frac{1}{2}(\frac{C_lR + C_rR - ( H_l +  H_r)R^2}{I_\omega + mR^2} ) \\
代入（1.6）:
&=\frac{1}{2}(\frac{C_lR + C_rR -M\ddot{x_p}R^2}{I_\omega + mR^2}) \\
代入（1.15）:
&= \frac{1}{2}(\frac{C_lR + C_rR -MR^2 \cdot( \ddot{x}  + L \ddot{\theta}  \cos \theta - L \dot{\theta}^2  \sin \theta)}{I_\omega + mR^2} ) \\
\end{split} 
$$

整理得：
$$
(\frac{2I_\omega}{R^2} + 2m + M)\ddot{x} + ML \ddot{\theta}  \cos \theta  - ML \dot{\theta}^2  \sin \theta = \frac{1}{R}(C_l + C_r)   \tag{2.1}
$$

---

# 3、获取$\ddot{\theta}$ 与状态的关系

由（1.7）得：
$$
\begin{split}
I_\theta \cdot \ddot{\theta} &= ( V_l +  V_r) L  \sin \theta - ( H_l +  H_r) \cdot L \cdot \cos \theta - (C_l + C_r) \\
代入（1.8）(1.5)：&= M（\ddot{z_p}+g）L \sin \theta - (\frac{1}{R}（C_l + C_r）- \frac{I_\omega + mR^2}{R^2} \cdot 2 \cdot\ddot{x}) \cdot L \cos \theta - (C_l + C_r) \\
代入(1.18)：&= M（- L \ddot{\theta}  \sin \theta - L \dot{\theta}^2  \cos \theta + g） L  \sin \theta + 2L\cos \theta(m+\frac{I_\omega}{R^2}) \cdot \ddot{x} - (1 +  \frac{L\cos\theta}{R})(C_l + C_r) \\
&= - ML^2 \ddot{\theta}  \sin^2 \theta - ML^2 \dot{\theta}^2  \cos \theta \sin \theta+ MgL \sin \theta + 2L\cos \theta(m+\frac{I_\omega}{R^2}) \cdot \ddot{x} - (1 +  \frac{L\cos\theta}{R})(C_l + C_r) \\
\end{split}
$$

整理得：
$$
I_\theta \cdot \ddot{\theta} = - ML^2 \ddot{\theta}  \sin^2 \theta - ML^2 \dot{\theta}^2  \cos \theta \sin \theta+ MgL \sin \theta + 2L\cos \theta(m+\frac{I_\omega}{R^2}) \cdot \ddot{x} - (1 +  \frac{L\cos\theta}{R})(C_l + C_r)
\tag{3.1}
$$

---

#  4、获取  $\ddot{\delta}$ 与状态的关系
由(1.9)得：

$$
\begin{split}
I_\delta \cdot \ddot{\delta} &= \frac{D}{2} \cdot ( H_r -  H_l) \\
代入(1.3)、(1.4)& = \frac{D}{2} \cdot ( \frac{C_rR - \ddot{x_r} (I_{\omega}+mR^2)}{R^2} - \frac{C_lR - \ddot{x_l} (I_{\omega}+mR^2)}{R^2} ) \\
& = \frac{D}{2} \cdot ( \frac{C_rR - C_lR }{R^2} - \frac{\ddot{x_r} (I_{\omega}+mR^2) - \ddot{x_l} (I_{\omega}+mR^2)}{R^2} ) \\
& = \frac{D}{2} \cdot ( \frac{C_r - C_l }{R} - \frac{(\ddot{x_r} -  \ddot{x_l})(I_{\omega}+mR^2)}{R^2} ) \\
代入(1.20)& = \frac{D}{2} \cdot ( \frac{C_r - C_l }{R} - \frac{\ddot{\delta}D(I_{\omega}+mR^2)}{R^2} ) \\
\end{split}
$$

整理得：
$$
(\frac{2I_{\delta} }{D} + \frac{DI_{\omega}}{R^2} + Dm) \cdot \ddot{\delta} =  \frac{1}{R}(C_r - C_l) \tag{4.1}
$$

---

# 5、模型线性化
在平衡点附近对系统进行线性化处理，因为在平衡点附近 $\theta \approx 0$，那么$\sin \theta \approx \theta, \cos \approx 1$,因此可以将（2.1）、（3.1）、（4.1）线性化得：

$$
(\frac{2I_\omega}{R^2} + 2m + M)\ddot{x} + ML \ddot{\theta}  = \frac{1}{R}(C_l + C_r)   \tag{5.1}
$$

$$
I_\theta \cdot \ddot{\theta} = MgL\theta + 2L(m + \frac{I_\omega}{R^2})\ddot{x}- (1 + \frac{L}{R})(C_l + C_r)
\tag{5.2}
$$

$$
(\frac{2I_{\delta} }{D} + \frac{DI_{\omega}}{R^2} + Dm) \cdot \ddot{\delta} =  \frac{1}{R}(C_r - C_l) \tag{5.3}
$$

将上式写成矩阵形式
$$
\begin{bmatrix}
M+2m+\frac{2I_\omega}{R^2} & ML & 0  \\
-2L(m+\frac{I_{\omega}}{R^2}) & I_{\theta} & 0  \\
0 & 0 & Dm+\frac{2I_{\delta}}{D} + \frac{DI_{\omega}}{R^2}  \\
\tag{5.4}
\end{bmatrix} \cdot 
%-----------------
\begin{bmatrix}
\ddot{x} \\
\ddot{\theta} \\
\ddot{\delta} \\
\end{bmatrix} = 
%-----------------
\begin{bmatrix}
0 & \frac{1}{R} & \frac{1}{R}  \\
MgL & -(1+\frac{L}{R}) & -(1+\frac{L}{R})  \\
0 & \frac{1}{R} & \frac{1}{R}  \\
\end{bmatrix} \cdot 
%-----------------
\begin{bmatrix}
\theta\\
C_l \\
C_r \\
\end{bmatrix}
$$

---

# 6、平衡状态的状态方程
$$
\textbf{x} = \begin{bmatrix} \dot{x}  & \dot{\delta} & \theta & \dot{\theta} \end{bmatrix}^T，
\textbf{u} = \begin{bmatrix} C_l  & C_r \end{bmatrix}^T
\tag{6.1}
$$

$$
\dot{\textbf{x} }= A\textbf{x}+B\textbf{u} \tag{6.2}
$$

$$
% X_dot
\begin{bmatrix}
\ddot{x} \\
\ddot{\delta} \\
\dot{\theta} \\
\ddot{\theta} \\
\end{bmatrix} =
% A
\begin{bmatrix}
0 & 0 & a_{13} & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & a_{43} & 0 \\
\end{bmatrix} \cdot 
% X
\begin{bmatrix}
\dot{x} \\
\dot{\delta} \\
\theta \\
\dot{\theta} \\
\end{bmatrix} + 
% B
\begin{bmatrix}
b_{11}  & b_{12} \\
b_{21}  & b_{22} \\
0  & 0 \\
b_{41}  & b_{42} \\
\end{bmatrix} \cdot 
% U
\begin{bmatrix}
C_l \\
C_r \\
\end{bmatrix} 
$$

令$
X = （2I_{\theta}R^2 + 2L^2MR^2）m + I_{\theta}MR^2 + 2I_{\theta}I_{\omega} + 2ML^2I_{\omega}
$

$$
a_{13} = - \frac{1}{X}R^2M^2L^2g
$$

$$
b_{11} = \frac{1}{X}(I_{\theta} + MRL + L^2M)R
$$

$$
b_{12} =  \frac{1}{X}(I_{\theta} + MRL + L^2M)R
$$

$$
b_{21} = - \frac{DR}{2I_{\delta}R^2 + D^2I_{\omega} + D^2mR^2}
$$

$$
b_{22} = \frac{DR}{2I_{\delta}R^2 + D^2I_{\omega} + D^2mR^2}
$$

$$
a_{43} = \frac{1}{X}(MR^2 + 2mR^2 + 2I_{\omega})MgL
$$

$$
b_{41} = -\frac{1}{X}(MR^2 + 2mR^2 + 2I_{\omega} + MRL)
$$

$$
b_{42} = -\frac{1}{X}(MR^2 + 2mR^2 + 2I_{\omega} + MRL)
$$

将参数值代入得到平衡位置的线性化状态方程：

$$
% X_dot
\begin{bmatrix}
\ddot{x} \\
\ddot{\delta} \\
\dot{\theta} \\
\ddot{\theta} \\
\end{bmatrix} =
% A
\begin{bmatrix}
0 & 0 & -12.36570858 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & 83.159390223 & 0 \\
\end{bmatrix} \cdot 
% X
\begin{bmatrix}
\dot{x} \\
\dot{\delta} \\
\theta \\
\dot{\theta} \\
\end{bmatrix} + 
% B
\begin{bmatrix}
17.157000486  & 17.157000486 \\
-59.94028  & 59.94028 \\
0  & 0 \\
-73.71420318  & -73.71420318 \\
\end{bmatrix} \cdot 
% U
\begin{bmatrix}
C_l \\
C_r \\
\end{bmatrix} 
$$

---

# 7、速度跟随状态的状态方程

期望状态：
$$
\textbf{x}_{des} = \begin{bmatrix} cmd\_vel\_x  & cmd\_vel\_{yaw} & 0 & 0 \end{bmatrix}^T
\tag{7.1}
$$

定义速度跟随状态：
$$
\tilde{\textbf{x}} = \textbf{x} - \textbf{x}_{des} = \textbf{x} - \begin{bmatrix} cmd\_vel\_x  & cmd\_vel\_{yaw} & 0 & 0 \end{bmatrix}^T
\tag{7.2}
$$

$$
\tilde{\textbf{x}} = \begin{bmatrix} vel\_x\_err  & vel\_{yaw}\_err & \theta & \dot{\theta} \end{bmatrix}^T
\tag{7.3}
$$

由(7.2)求导得：
$$
\begin{split}
\dot{\tilde{\textbf{x}}} &= \dot{\textbf{x}} - \dot{\textbf{x}_{des}} \\
(\textbf{x}_{des} 为 常数项) &= \dot{\textbf{x}} \\
代入(6.2)&=  A\textbf{x}+B\textbf{u} \\
&=  A(\textbf{x} - \textbf{x}_{des}) + B\textbf{u} + A\textbf{x}_{des} \\
代入（7.2)&=  A\tilde{\textbf{x}} + B\textbf{u} + A\textbf{x}_{des} \\
\end{split}
$$

观察 $A$ 矩阵与 $\textbf{x}_{des}$ 结构可以得到 $A\textbf{x}_{des} = 0$

因此
$$
\dot{\tilde{\textbf{x}}} = A\tilde{\textbf{x}} + B\textbf{u}
$$

由此可知速度跟随状态的$A、B$矩阵与平衡状态的相同，速度跟随状态下的状态方程为
$$
% X_dot
\begin{bmatrix}
\dot{vel\_x\_err} \\
\dot{vel\_{yaw}\_err} \\
\dot{\theta} \\ 
\ddot{\theta} 
\end{bmatrix} =
% A
\begin{bmatrix}
0 & 0 & -12.36570858 & 0 \\
0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 \\
0 & 0 & 83.159390223 & 0 \\
\end{bmatrix} \cdot 
% X
\begin{bmatrix}
vel\_x\_err \\
vel\_{yaw}\_err \\
\theta \\ 
\dot{\theta} 
\end{bmatrix} + 
% B
\begin{bmatrix}
17.157000486  & 17.157000486 \\
-59.94028  & 59.94028 \\
0  & 0 \\
-73.71420318  & -73.71420318 \\
\end{bmatrix} \cdot 
% U
\begin{bmatrix}
C_l \\
C_r \\
\end{bmatrix} 
$$

---

# 8、参考
[1] 两轮自平衡机器人LQR-模糊控制算法研究 李志超
[2] https://www.youtube.com/watch?v=bJM9jU-P_H0&t=311s&ab_channel=mouhknowsbest