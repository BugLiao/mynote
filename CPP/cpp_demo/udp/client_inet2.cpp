/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
constexpr int SERVER_PORT = 8000;
constexpr char SERVER_IP[] = "192.168.3.2";

int main() {
  /* socket文件描述符 */
  int sock_fd;
  struct sockaddr_in server {};
  /* 建立udp socket */
  sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock_fd < 0) {
    std::cout << "socket error" << std::endl;
    exit(1);
  }

  //  socklen_t server_address_size = sizeof(server);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr(SERVER_IP);
  server.sin_port = htons(SERVER_PORT);

  int send_num;
  //  int recv_num;
  char send_buf[20] = "hey, I`m BB";
  //  char recv_buf[20];

  std::cout << "client send: " << send_buf << std::endl;
  send_num = sendto(sock_fd, send_buf, strlen(send_buf), 0, (struct sockaddr *)&server, sizeof(server));

  if (send_num < 0) {
    std::cout << "sendto error: " << std::endl;
    exit(1);
  }

  //  recv_num = recvfrom(sock_fd, recv_buf, sizeof(recv_buf), 0, (struct sockaddr *)&server, &server_address_size);
  //
  //  if (recv_num < 0) {
  //    std::cout << "recvfrom error: " << std::endl;
  //    exit(1);
  //  }
  //
  //  std::cout << "client receive " << recv_num << " bytes " << recv_buf << std::endl;
  close(sock_fd);

  return 0;
}