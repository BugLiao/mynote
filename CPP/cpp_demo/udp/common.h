/*******************************************************************************
 * Copyright (c) 2022/3/20, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#pragma once

constexpr uint8_t TYPE_A = 0x01;
constexpr uint8_t TYPE_B = 0x02;
constexpr uint8_t TYPE_C = 0x03;

typedef struct {
  uint8_t data_type;

} Header;

typedef struct {
  double x;
  double y;
  double z;
} Vel;

typedef struct {
  Header header;
  Vel vel;
} TypeA;

typedef struct {
  Header header;
} TypeB;

typedef struct {
  int id;
  double x;
  std::string name;
} TypeC;