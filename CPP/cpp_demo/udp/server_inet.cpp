/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdlib>
#include <cstring>
#include <iostream>

#include "common.h"
constexpr int SERV_PORT = 8000;

int main() {
  /* socket文件描述符 */
  int sock_fd;
  /* 设置address */
  struct sockaddr_in server {};
  struct sockaddr_in client {};

  /* 建立udp socket */
  if ((sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) {
    std::cout << "socket error" << std::endl;
    exit(1);
  };

  server.sin_family = AF_INET;
  server.sin_port = htons(SERV_PORT);
  server.sin_addr.s_addr = htonl(INADDR_ANY);  // IP地址，需要进行网络序转换，INADDR_ANY：本地地址

  /* 绑定socket */
  if (bind(sock_fd, (struct sockaddr *)&server, sizeof(server)) < 0) {
    std::cout << "bind error" << std::endl;
    exit(1);
  }

  int recv_num;
  char recv_buf[1000];

  while (true) {
    std::cout << "========================" << std::endl;
    socklen_t client_address_size = sizeof(client);
    recv_num = recvfrom(sock_fd, &recv_buf, 100, 0, (struct sockaddr *)&client, &client_address_size);

    std::cout << "recv_num: " << recv_num << std::endl;
    char *name = inet_ntoa(client.sin_addr);
    std::string Ip = "192.168.3.2";
    if (strcmp(name, Ip.c_str()) == 0) {
      std::cout << "the client is 192.168.3.2" << std::endl;
    } else {
      std::cout << "the client is other" << std::endl;
    }

    if (recv_num < 0) {
      std::cout << "recvfrom error" << std::endl;
      exit(1);
    }

    if (recv_buf[0] == TYPE_A) {
      std::cout << "get TYPE_A" << std::endl;
    } else if (recv_buf[0] == TYPE_B) {
      std::cout << "get TYPE_B" << std::endl;
    } else {
      std::cout << "what the fuck type" << std::endl;
    }

    //    std::cout << recv_buf.header.data_type << std::endl;
    //    std::cout << recv_buf << std::endl;

    TypeC recv_bufc;
    memcpy(&recv_bufc.id, recv_buf, sizeof(recv_bufc.id));
    memcpy(&recv_bufc.x, recv_buf + sizeof(recv_bufc.id), sizeof(recv_bufc.x));
    //    memcpy(data1, recv_buf + 1, sizeof(data1));  // 4,5,6,7
    char name1[100];
    memcpy(name1, recv_buf + sizeof(recv_bufc.id) + sizeof(recv_bufc.x), strlen(recv_bufc.name.c_str()));
    recv_bufc.name.append(name1, strlen(recv_bufc.name.c_str()));
    std::cout << "recv_bufc.id: " << recv_bufc.id << std::endl;
    std::cout << "recv_bufc.x: " << recv_bufc.x << std::endl;
    std::cout << "recv_bufc.name: " << recv_bufc.name << std::endl;
    //    TypeA *data2 = (TypeA *)data1;
    //    std::cout << strlen(recv_buf) << std::endl;
    //    std::cout << recv_buf << std::endl;
    //    std::cout << data2->vel.y << std::endl;
    //    std::cout << data2->vel.z << std::endl;
  }

  close(sock_fd);

  return 0;
}