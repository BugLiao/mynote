/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <cstdlib>
#include <cstring>
#include <iostream>

#include "common.h"

constexpr int SERVER_PORT = 8000;
constexpr char SERVER_IP[] = "127.0.0.1";

int main() {
  /* socket文件描述符 */
  int sock_fd;
  struct sockaddr_in server {};
  /* 建立udp socket */
  sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock_fd < 0) {
    std::cout << "socket error" << std::endl;
    exit(1);
  }

  //  socklen_t server_address_size = sizeof(server);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr(SERVER_IP);
  server.sin_port = htons(SERVER_PORT);

  int send_num;
  TypeA send_bufa;
  send_bufa.header.data_type = TYPE_A;
  send_bufa.vel.x = 1.1;
  send_bufa.vel.y = 2.2;
  send_bufa.vel.z = 3.3;

  TypeB send_bufb;
  send_bufb.header.data_type = TYPE_B;

  TypeC send_bufc;
  send_bufc.id = 568;
  send_bufc.x = 2.345;
  //  strcpy(send_bufc.name, "dajiji");
  send_bufc.name = "dajiji";
  char ByteArray[100];
  int count = 0;
  memcpy(ByteArray, &send_bufc.id, sizeof(send_bufc.id));
  count += sizeof(send_bufc.id);
  memcpy(ByteArray + count, &send_bufc.x, sizeof(send_bufc.x));
  count += sizeof(send_bufc.x);
  memcpy(ByteArray + count, send_bufc.name.c_str(), strlen(send_bufc.name.c_str()));
  count += strlen(send_bufc.name.c_str());
  send_num = sendto(sock_fd, &ByteArray, count, 0, (struct sockaddr *)&server, sizeof(server));

  //  send_num = sendto(sock_fd, &send_bufc, sizeof(send_bufc), 0, (struct sockaddr*)&server, sizeof(server));
  std::cout << sizeof(send_bufc.id) << std::endl;
  std::cout << sizeof(send_bufc.x) << std::endl;
  std::cout << strlen(send_bufc.name.c_str()) << std::endl;
  std::cout << send_num << std::endl;
  if (send_num < 0) {
    std::cout << "sendto error: " << std::endl;
    exit(1);
  }
  close(sock_fd);

  std::string ss;
  char aa1[100];
  strcpy(aa1, "dajiji");

  ss.append(aa1, 6);

  std::cout << ss << std::endl;
  return 0;
}