#include <netinet/in.h>

#include <cstring>
#include <iostream>
#include <string>
#include <typeinfo>
#include <vector>
#define PORT 9734

using namespace std;
typedef struct {
  double x;
  double y;
  double z;
} Vel;

int main() {
  vector<int> v1{1, 2, 3};
  for (const auto &item : v1) {
    cout << item << endl;
  }
  //  cout << v1.front() << endl;
  //  cout << v1.back() << endl;

  auto aa = v1.begin();

  while (aa != v1.end()) {
    cout << *aa << endl;
    v1.erase(aa);
    aa++;
  }
  return 0;
}
