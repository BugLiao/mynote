# socket TCP

## server 流程

1. `socket`：建立一个socket。

2. `bind`： 将这个socket绑定在某个文件上（AF_UNIX）或某个端口上（AF_INET），我们会分别介绍这两种。

3. `listen`：开始监听

4. `accept`：如果监听到客户端连接，则调用accept接收这个连接并同时新建一个socket来和客户进行通信

5. `read/write`：读取或发送数据到客户端

6. `close`：通信完成后关闭socket

## client 流程

1. `socket`：建立一个socket

2. `connect`：主动连接服务器端的某个文件（AF_UNIX）或某个端口（AF_INET）

3. `read/write`：如果服务器同意连接（accept），则读取或发送数据到服务器端

4. `close`：通信完成后关闭socket

---

# 收发函数

read/write

```c++
ssize_t write(int fd, const void*buf,size_t nbytes);
ssize_t read(int fd,void *buf,size_t nbyte);
```

recv/send

```c++
int recv(int sockfd,void *buf,int len,int flags);
int send(int sockfd,void *buf,int len,int flags);
```

前面的三个参数和read, write一样,第四个参数可以是0或者是以下的组合,如果flags为0,则和read,write一样的操作

MSG_DONTROUTE - 不查找表

MSG_OOB - 接受或者发送带外数据

MSG_PEEK - 查看数据,并不从系统缓冲区移走数据

MSG_WAITALL - 等待所有数据

---

# bind

```c++
int bind(int socket, const struct sockaddr * address, size_t address_len)
```

如果我们使用AF_UNIX来创建socket，相应的地址格式是sockaddr_un，而如果我们使用AF_INET来创建socket，相应的地址格式是sockaddr_in，因此我们需要将其强制转换为sockaddr这一通用的地址格式类型，而sockaddr_un中的sun_family和sockaddr_in中的sin_family分别说明了它的地址格式类型，因此bind()
函数就知道它的真实的地址格式。第三个参数address_len则指明了真实的地址格式的长度。

bind()函数正确返回0，出错返回-1

---

# listen

```c++
int listen(int socket, int backlog)
```

backlog：等待连接的最大个数，如果超过了这个数值，则后续的请求连接将被拒绝

listen()函数正确返回0，出错返回-1

# 接收请求

```c++
int accept(int socket, struct sockaddr * address, size_t * address_len)
```

同样，第二个参数也是一个通用地址格式类型，这意味着我们需要进行强制类型转化

这里需要注意的是，address是一个传出参数，它保存着接受连接的客户端的地址，如果我们不需要，将address置为NULL即可。

address_len：我们期望的地址结构的长度，注意，这是一个传入和传出参数，传入时指定我们期望的地址结构的长度，如果多于这个值，则会被截断，而当accept()函数返回时，address_len会被设置为客户端连接的地址结构的实际长度。

另外如果没有客户端连接时，accept()函数会阻塞

accept()函数成功时返回新创建的socket描述符，出错时返回-1

---

# connect

客户端通过connect()函数与服务器连接：

```c++
int connect(int socket, const struct sockaddr * address, size_t address_len)
```

对于第二个参数，我们同样需要强制类型转换

address_len指明了地址结构的长度

connect()函数成功时返回0，出错时返回-1

双方都建立连接后，就可以使用常规的read/write函数来传递数据了

# close()

通信完成后，我们需要关闭socket：

int close(int fd)

close是一个通用函数（和read，write一样），不仅可以关闭文件描述符，还可以关闭socket描述符



