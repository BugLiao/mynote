cmake_minimum_required(VERSION 3.10)
project(cpp_demo)

## Use C++14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
#add_definitions(-Wall -Werror -Wno-enum-compare)

add_subdirectory(template)

find_package(Protobuf REQUIRED)
include_directories(
        protocol_buffer/proto
        ${Protobuf_INCLUDE_DIRS}
)


add_executable(main main.cpp)

# TCP
add_executable(tcp_server_unix tcp/server_unix.cpp)
add_executable(tcp_client_unix tcp/client_unix.cpp)

add_executable(tcp_server_inet tcp/server_inet.cpp)
add_executable(tcp_client_inet tcp/client_inet.cpp)

# UDP
add_executable(udp_server_inet udp/server_inet.cpp)
add_executable(udp_client_inet udp/client_inet.cpp)
add_executable(udp_client_inet2 udp/client_inet2.cpp)

# protocol buffer
file(GLOB_RECURSE proto_source "protocol_buffer/proto/*.cc")

add_executable(proto_client protocol_buffer/proto_client.cpp ${proto_source})
target_link_libraries(proto_client
        ${Protobuf_LIBRARIES}
        )

add_executable(proto_server protocol_buffer/proto_server.cpp ${proto_source})
target_link_libraries(proto_server
        ${Protobuf_LIBRARIES}
        )
# Class
add_executable(test_class class/test_class.cpp class/class_define.h)

# ========================= robot communicate ==================================

add_executable(robot_client robot_communicate/robot_client.cpp ${proto_source})
target_link_libraries(robot_client
        ${Protobuf_LIBRARIES}
        )

add_executable(robot_server robot_communicate/robot_server.cpp ${proto_source})
target_link_libraries(robot_server
        ${Protobuf_LIBRARIES}
        )