/*******************************************************************************
 * Copyright (c) 2022/3/20, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <iostream>

#include "class_define.h"
int main(int argc, char** argv) {
  std::cout << "hello world !!!" << std::endl;
  Base base;
  ClassA a;

  Base* B_a = new ClassA();
  base.sayHi();
  a.sayHi();
  B_a->sayHi();

  base.sayHello();
  a.sayHello();
  B_a->sayHello();

  int num = 8;
  a.saySomeThing(num);
  double numq = 9;
  a.saySomeThing(numq);
  return 0;
}