/*******************************************************************************
 * Copyright (c) 2022/3/31, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <iostream>

template <typename T>
void myAdd(T a, T b) {
  T c = a + b;
  std::cout << typeid(c).name() << std::endl;
  std::cout << c << std::endl;
}

template <typename T1, typename T2>
void myAdd2(T1 a, T2 b) {
  T1 c = a + b;
  std::cout << typeid(c).name() << std::endl;
  std::cout << c << std::endl;
}

int main(int argc, char** argv) {
  int a = 1, b = 2;
  double c = 3.5, d = 4.5;
  myAdd(a, b);
  myAdd(c, d);

  myAdd2(a, d);
  myAdd2(c, b);
  return 0;
}