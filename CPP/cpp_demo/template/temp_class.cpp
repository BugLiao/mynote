/*******************************************************************************
 * Copyright (c) 2022/3/31, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <iostream>

template <class T>
class TestClass {
 public:
  void func() { obj.show(); }

  T obj;
};

template <class T1, class T2>
class TestClass2 {
 public:
  void func() {
    obj1.show();
    obj2.show();
  }

  T1 obj1;
  T2 obj2;
};

template <class T1, class T2>
class TestClass3 {
 public:
  void func() {
    obj1.show();
    std::cout << number << std::endl;
  }

  T1 obj1;
  T2 number = 5.5;
};

class Person1 {
 public:
  void show() { std::cout << "Person1" << std::endl; }
};

class Person2 {
 public:
  void show() { std::cout << "Person2" << std::endl; }
};

class Person3 {
 public:
  Person3() {}
  void show() { std::cout << "Person2" << std::endl; }
};

int main(int argc, char** argv) {
  //  TestClass<Person1> test_class1{};
  //  test_class1.func();
  //
  //  TestClass<Person2> test_class2{};
  //  test_class2.func();
  //
  //  TestClass2<Person1, Person2> test_class3{};
  //  test_class3.func();

  int num = 5;
  TestClass3<Person1, int> test_class4{};
  test_class4.func();
  return 0;
}