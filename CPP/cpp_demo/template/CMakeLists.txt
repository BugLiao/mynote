cmake_minimum_required(VERSION 3.10)
project(template)

## Use C++14
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
#add_definitions(-Wall -Werror -Wno-enum-compare)

add_executable(temp_fun temp_fun.cpp)
add_executable(temp_class temp_class.cpp)