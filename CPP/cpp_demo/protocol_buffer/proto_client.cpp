/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <iostream>

#include "common.h"
#include "robot.pb.h"

constexpr int SERVER_PORT = 8000;
constexpr char SERVER_IP[] = "127.0.0.1";
constexpr int BUFFLEN = 200;
int main() {
  /* socket文件描述符 */
  int sock_fd;
  struct sockaddr_in server {};
  /* 建立udp socket */
  sock_fd = socket(AF_INET, SOCK_DGRAM, 0);
  if (sock_fd < 0) {
    std::cout << "socket error" << std::endl;
    exit(1);
  }

  //  socklen_t server_address_size = sizeof(server);
  server.sin_family = AF_INET;
  server.sin_addr.s_addr = inet_addr(SERVER_IP);
  server.sin_port = htons(SERVER_PORT);

  int send_num;
  char send_buffer[BUFFLEN];

  //  robot::CmdVel cmd_vel;
  //  cmd_vel.mutable_header()->set_frame_id("rapper");
  //  cmd_vel.mutable_header()->set_data_type(CMD_VEL);
  //  cmd_vel.set_x(1.1);
  //  cmd_vel.set_y(2.22);
  //  cmd_vel.set_z(3.333);
  //  cmd_vel.SerializeToArray(send_buffer, cmd_vel.ByteSizeLong());
  //  send_num = sendto(sock_fd, &send_buffer, cmd_vel.ByteSizeLong(), 0, (struct sockaddr *)&server, sizeof(server));
  //
  robot::State state;
  state.mutable_header()->set_frame_id("rapper");
  state.mutable_header()->set_data_type(STATE);
  state.set_x(4.4);
  state.set_y(5.5);
  state.set_z(6.6);
  state.SerializeToArray(send_buffer, state.ByteSizeLong());
  send_num = sendto(sock_fd, &send_buffer, state.ByteSizeLong(), 0, (struct sockaddr *)&server, sizeof(server));

  //  robot::Imu imu;
  //  imu.mutable_header()->set_frame_id("rapper");
  //  imu.mutable_header()->set_data_type(IMU);
  //  imu.set_roll(7.7);
  //  imu.set_pitch(8.8);
  //  imu.set_yaw(9.9);
  //  imu.SerializeToArray(send_buffer, imu.ByteSizeLong());
  //  send_num = sendto(sock_fd, &send_buffer, imu.ByteSizeLong(), 0, (struct sockaddr *)&server, sizeof(server));

  if (send_num < 0) {
    std::cout << "sendto error: " << std::endl;
    exit(1);
  }
  close(sock_fd);
  return 0;
}