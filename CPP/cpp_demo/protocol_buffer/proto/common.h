/*******************************************************************************
 * Copyright (c) 2022/3/21, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#pragma once
constexpr int CMD_VEL = 1;
constexpr int STATE = 2;
constexpr int IMU = 3;