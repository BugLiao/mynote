/*******************************************************************************
 * Copyright (c) 2022/3/19, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include <cstdlib>
#include <iostream>
int main() {
  /* create a socket */
  int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);

  struct sockaddr_un address {};
  address.sun_family = AF_UNIX;
  strcpy(address.sun_path, "server_socket");

  /* connect to the server */
  int result = connect(sockfd, (struct sockaddr *)&address, sizeof(address));
  if (result == -1) {
    std::cout << "connect failed" << std::endl;
    exit(1);
  }

  /* exchange data */
  char ch = 'A';
  send(sockfd, &ch, 1, 0);
  recv(sockfd, &ch, 1, 0);
  std::cout << "get char from server: " << ch << std::endl;
  /* close the socket */
  close(sockfd);

  return 0;
}