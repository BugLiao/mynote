# socket UDP

## 服务端的步骤如下：

1. socket： 建立一个socket

2. bind： 将这个socket绑定在某个端口上（AF_INET）

3. recvfrom： 如果没有客户端发起请求，则会阻塞在这个函数里

4. close： 通信完成后关闭socket

## 客户端的步骤如下：

1. socket： 建立一个socket

2. sendto： 相服务器的某个端口发起请求（AF_INET）

3. close： 通信完成后关闭socket

---

# 收发函数

```c++
int recvfrom(int sockfd, void * buf, size_t len, int flags, struct sockaddr * src_addr, socklen_t * addrlen);

int sendto(int sockfd, const void * buf, size_t len, int flags, const struct sockaddr * dest_addr, socklen_t addrlen);
```

recvfrom和sendto都是阻塞

## recvfrom：

return: 接收到的字节数，错误为-1

socket：socket套接字

buf：接收buffer的地址

len：接收buffer的大小

flags：一般情况下为0

src_addr：客户端的地址，可以通过它知道是谁发的消息

addrlen：客户端地址的长度，与地址域有关

## sendto：

return： 发送的字节数

socket：socket套接字

buf：发送buffer的地址

len：发送buffer的大小

flags：一般情况下为0

dest_addr：服务端的地址

addrlen：服务端地址长度，与地址域有关

