//
// Created by liao on 2021/8/11.
//


#ifndef PY_CPP_FOO_H
#define PY_CPP_FOO_H
#include <iostream>


namespace Foo
{
    class TestLib
    {
    public:
        TestLib(){}

        void display();
        void display(int a);
    };


    extern "C" {
        TestLib obj;
        void display() {
            obj.display();
        }
        void display_int(int i) {
           obj.display(i);
       }
    }

} // end namespace Foo

#endif //PY_CPP_FOO_H
