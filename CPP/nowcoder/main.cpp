#include <iostream>
#include <vector>

#include "algorithm"
#include "hello.h"

void myp(int value) { std::cout << value << std::endl; }
int main() {
  std::vector<int> vi;
  vi.push_back(1);
  vi.push_back(2);
  vi.push_back(3);

  std::cout << sizeof(vi) << std::endl;
  std::cout << vi.size() << std::endl;
  //  std::cout << size(vi) << std::endl;
  //  std::for_each(vi.begin(), vi.end(), myp);
  return 0;
}
