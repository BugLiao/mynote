/*******************************************************************************
 * Copyright (c) 2022/3/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include <iostream>

#include "hello.h"
int main(int argc, char** argv) {
  std::cout << "hello world !!!" << std::endl;
  hello();
  return 0;
}