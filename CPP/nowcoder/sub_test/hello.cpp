/*******************************************************************************
 * Copyright (c) 2022/3/28, Liao LunJia.
 * All rights reserved.
 *******************************************************************************/

#include "hello.h"

#include <iostream>

void hello() { std::cout << "hello world !!!" << std::endl; }