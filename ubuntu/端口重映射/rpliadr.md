# 通过设备ID，将`ttyusb0`映射成`rplidar`

## 1、查看设备的ID
```bash
lsusb
```
![](PIC/01.png)

## 2、编写端口映射的规则
```bash
cd /etc/udev/rules.d/
sudo touch rplidar.rules
sudo vim rplidar.rules
```

根据上面查到的设备ID在rplidar.rules写入
```rules
KERNEL=="ttyUSB*", ATTRS{idVendor}=="10c4", ATTRS{idProduct}=="ea60", MODE:="0777", SYMLINK+="rplidar" 
```
## 3、重启电脑
```bash
reboot
```

## 4、输入一下指令，检查是否映射成功
```bash
ls -l /dev | grep ttyUSB
```
![](PIC/02.png)
