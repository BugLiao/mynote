# Ubuntu 开机自启

# note: 开机自启要设置免密码
[https://gaomf.cn/2018/11/17/Sudo_No_Passwd/]

## 1、创建需要开自启的文件，本例开启src里面的`hello_world.cpp`

## 2、创建自启脚本文件夹 `scripts`
![pic](./pic/04.png)
## 3、开机自启项设置

### 3.1 创建 `start_master.sh`
```shell
#!/bin/bash
source /opt/ros/noetic/setup.bash
export ROS_PACKAGE_PATH=~/catkin_ws:$ROS_PACKAGE_PATH
source ~/catkin_ws/devel/setup.bash
export ROS_IP=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1' | grep -v '172.17.0.1')
if test -z "${ROS_IP}"; then
  export ROS_IP=$(dig +short localhost)
fi
roscore
```
### 3.2 创建 `start_master.service`
**note:** 一般情况下，一个`.service`文件对应一个`.sh`文件，例如，`start_master.service`负责执行`start_master.sh`
，`.service`文件怎么写要百度一下，规则很简单
```service
[Unit]
Description=auto start ros master
After=network-online.target
[Service]
User=liao
Type=simple
#Type=forking
ExecStart=/bin/bash -c "~/start_master.sh"
[Install]
WantedBy=multi-user.target
```
### 3.3 创建 `hello_world_start.sh`
```shell
#!/bin/bash
source ~/catkin_ws/devel/setup.bash
rosrun auto_start hello_world
```

### 3.4 创建 `pub_tf_start.service`
```shell
[Unit]
Description=auto start pub_tf
After=network-online.target
[Service]
User=liao
Type=simple
#Type=forking
ExecStart=/bin/bash -c "~/hello_world_start.sh"
[Install]
WantedBy=multi-user.target
```

## 4、创建自启配置文件 `auto_start_config.sh`
```shell
#!/bin/bash
# shellcheck disable=SC2046

# 将两个sh文件复制到 ~/ 路径下
cp $(rospack find auto_start)/scripts/start_master.sh ~/
cp $(rospack find auto_start)/scripts/hello_world_start.sh ~/

# 将两个sh文件权限，使其具有可执行的权限
chmod 777 ~/start_master.sh
chmod 777 ~/hello_world_start.sh

# 将两个.service 文件拷贝到相应的系统文件夹
sudo cp $(rospack find auto_start)/scripts/start_master.service  /lib/systemd/system/
sudo cp $(rospack find auto_start)/scripts/hello_world_start.service  /lib/systemd/system/

# 开启这两个服务的开机自启动
sudo systemctl enable start_master.service
sudo systemctl enable hello_world_start.service

echo "Finish "
```

## 5、 使能自启服务
在`scripts` 路径下开启终端执行以下命令
```bash
sh auto_start_config.sh
```
## 6、 重启电脑
重启后，运行`rostopic list`
可以看到`roscore` 和节点 `helloworld` 已经启动，并已经在发布话题`chatter`  
![pic](./pic/01.png)

监听话题 `chatter`  
```bash
rostopic echo /chatter
```
![pic](./pic/02.png)

这时就证明自启成功。

## 7、 关闭这两个自启服务

### 7.1 创建 `delete_auto_start_service.sh`
```shell
#!/bin/bash

echo "Start to remove script files"

rm  ~/start_master.sh
rm  ~/hello_world_start.sh

echo " "
echo "Start to remove service files from /lib/systemd/system/"
echo ""
sudo rm  /lib/systemd/system/start_master.service
sudo rm  /lib/systemd/system/hello_world_start.service
echo " "
echo "Disable auto start service ! "
echo ""
sudo systemctl disable start_master.service
sudo systemctl disable hello_world_start.service
echo "Finish"
```
### 7.2 在上面文件的目录下执行
```bash
sh delete_auto_start_service.sh
```
![pic](./pic/03.png)

关闭自启服务成功