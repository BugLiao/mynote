#!/bin/bash

echo "Start to remove script files"

rm  ~/start_master.sh
rm  ~/hello_world_start.sh

echo " "
echo "Start to remove service files from /lib/systemd/system/"
echo ""
sudo rm  /lib/systemd/system/start_master.service
sudo rm  /lib/systemd/system/hello_world_start.service
echo " "
echo "Disable auto start service ! "
echo ""
sudo systemctl disable start_master.service
sudo systemctl disable hello_world_start.service
echo "Finish"
